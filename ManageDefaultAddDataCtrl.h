/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef MANAGEDEFAULTADDDATACTRL_H
#define MANAGEDEFAULTADDDATACTRL_H

#include <QWidget>
#include "ui_ManageDefaultAddDataCtrl.h"
#include <QueryCtrl.h>
class ManageDefaultAddDataCtrl : public GenericCtrl, public Ui::ManageDefaultAddDataCtrl
{
    Q_OBJECT

public:
    explicit ManageDefaultAddDataCtrl(QWidget* pParent);
    ~ManageDefaultAddDataCtrl();

private:




};

#endif // MANAGESERVICESGROUPCTRL_H
