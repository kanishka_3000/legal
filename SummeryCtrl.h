/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef SUMMERYCTRL_H
#define SUMMERYCTRL_H

#include "ui_SummeryCtrl.h"
#include <GenericCtrl.h>
#include <Case.h>
#include <Invoice.h>
#include <core/InvoiceSummeryReportHandler.h>
#include <set>

class SummeryCtrl : public GenericCtrl, private Ui::SummeryCtrl
{
    Q_OBJECT

public:
    explicit SummeryCtrl(QWidget *parent = 0);

    // GenericCtrl interface
public:
    virtual void OnCreateCtrl();
    void ExtractFormInfo();
    InvoiceSummeryReportHandler::ReportType GetSelectedReportType();
private:
    void CheckItems(bool bChecked);
    void CollectFieldsIfChecked(QCheckBox* pCheckBox, QString sField , QList<QString>& lstFields);
    void AddCSVFieldMap(QString sFrom, QString sTo);
    QString s_CustomerName;
    QString s_DateField;
    QString s_StartDate;
    QString s_EndDate;

    QList<std::shared_ptr<Case>> lst_Cases;
    QMap<QString, QListWidgetItem*> map_ListWidgetItems;
    bool b_Initialized;
    QSet<int> set_InvoiceTypes;
    QSet<int> set_CaseStates;

    QMap<QString, QString> map_CSVFieldMappings;
    QMap<QString, std::set<std::shared_ptr<Invoice>> > map_CaseUpdateByCase;
public slots:
    void OnGenerate();
    void OnGenerateCSV();
    //void OnShowCases(QString sText);
    void OnCheckAll();
    void OnUncheckAll();
    void OnSearchCase();

    void OnFindUpdatedCases();
};

class InvoiceDataSummeryProvider:public InvoiceSummeryDataProvider
{
public:
    InvoiceDataSummeryProvider(QMap<QString, QList<std::shared_ptr<Invoice>>>& mapData);

    // InvoiceSummeryDataProvider interface
public:
    virtual bool SetCurrentRow(int iRow) const;
    virtual int GetColumnCount() const;
    virtual int GetRowCount() const;
    virtual QString GetData(QString sColumn) const;
};

#endif // SUMMERYCTRL_H
