/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef ADDCASECTRL_H
#define ADDCASECTRL_H

#include <QWidget>
#include "ui_AddCaseCtrl.h"
#include <EntityManipulaterCtrl.h>
class AddCaseCtrl : public EntityManipulaterCtrl, EntityManipulaterCtrl::PopulationCallback, public Ui::AddCaseCtrl
{
    Q_OBJECT

public:
    explicit AddCaseCtrl(QWidget *parent = 0);
    ~AddCaseCtrl();
    void OnCreateCtrl();
    virtual void PopulateWithEntity(std::shared_ptr<Entity> pEntity);
    virtual void OnPreSave();
public slots:
    void OnClose();
private:


    // GenericCtrl interface
public:
    virtual void OnKey(QString sKey);

    // EntityManipulaterCtrl interface
    void Reset();
protected:
    virtual void OnPostSave() override;

    // PopulationCallback interface
public:
    virtual void OnPopulation(EntityManipulaterCtrl::PopulationFeedback eType, QString sFieldName, QWidget *pWidget) override;
};

#endif // ADDSERVICEGROUPCTRL_H
