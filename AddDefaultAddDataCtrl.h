/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef ADDDEFAULTADDDATACTRL_H
#define ADDDEFAULTADDDATACTRL_H

#include <QWidget>
#include "ui_AddDefaultAddDataCtrl.h"
#include <EntityManipulaterCtrl.h>
class AddDefaultAddDataCtrl : public EntityManipulaterCtrl, public Ui::AddDefaultAddDataCtrl
{
    Q_OBJECT

public:
    explicit AddDefaultAddDataCtrl(QWidget *parent = 0);
    ~AddDefaultAddDataCtrl();
    void OnCreateCtrl();
    virtual void PopulateWithEntity(std::shared_ptr<Entity> pEntity);
    virtual void OnPreSave();

public slots:
    void OnClose();
private:


    // EntityManipulaterCtrl interface
protected:
    virtual void OnPostSave() override;
};

#endif // ADDSERVICEGROUPCTRL_H
