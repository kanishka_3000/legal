/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef MANAGESERVICESGROUPCTRL_H
#define MANAGESERVICESGROUPCTRL_H

#include <QWidget>
#include "ui_ManageInvoiceDataCtrl.h"
#include <QueryCtrl.h>
class ManageInvoiceDataCtrl : public GenericCtrl, public Ui::ManageServicesGroupCtrl
{
    Q_OBJECT

public:
    explicit ManageInvoiceDataCtrl(QWidget* pParent);
    ~ManageInvoiceDataCtrl();

private:



};

#endif // MANAGESERVICESGROUPCTRL_H
