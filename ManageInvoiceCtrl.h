/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef MANAGEINVOICECTRL_H
#define MANAGEINVOICECTRL_H

#include <QWidget>
#include "ui_ManageInvoiceCtrl.h"
#include <QueryCtrl.h>
class ManageInvoiceCtrl : public GenericCtrl, public QueryCtrlCommandHandler ,public Ui::ManageInvoiceCtrl
{
    Q_OBJECT

public:
    explicit ManageInvoiceCtrl();
    ~ManageInvoiceCtrl();

private:
public slots:
    void OnCommandCreateInvoice();


    // GenericCtrl interface
public:
    virtual void OnCreateCtrl();

    // QueryCtrlCommandHandler interface
public:
    virtual int GetCommandCount();
    virtual QAction *GetAction(int iActionNo);
    virtual void FixButton(int iActionNo, QPushButton *pButton);
};

#endif // MANAGESERVICESGROUPCTRL_H
