/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "ManageInvoiceCtrl.h"
#include <LGLDBFields.h>
#include <GenericWnd.h>
#include <core/InvoiceReportHandler.h>
ManageInvoiceCtrl::ManageInvoiceCtrl() :
    GenericCtrl(nullptr)
{
    setupUi(this);

    p_QueryCtrl->SetCommandHanlder(this);
    p_QueryCtrl->SetTableName(TBLNM_LGL_INVOICE);
    p_QueryCtrl->SetKeyName(FDNM_LGL_INVOICE_INVOICETRACKID);
    p_QueryCtrl->SetOrderColumn(FDNM_LGL_INVOICE_INVOICE_NO);
    p_QueryCtrl->SetEnableExport(true);
}

ManageInvoiceCtrl::~ManageInvoiceCtrl()
{

}



void ManageInvoiceCtrl::OnCommandCreateInvoice()
{
    QString sKey = p_QueryCtrl->GetSelectedKey();
    std::unique_ptr<InvoiceReportHandler> pRep( new InvoiceReportHandler(this));
    pRep->Init(p_Parent->GetApplication()->GetConfig(), sKey);
}

void ManageInvoiceCtrl::OnCreateCtrl()
{
    connect(p_Filter, SIGNAL(NotifyFilter(QList<FilterCrieteria>)), p_QueryCtrl, SLOT(OnFilter(QList<FilterCrieteria>)));
    connect(p_QueryCtrl,SIGNAL(NotifyNewRequest()), p_Parent, SLOT(OnAddService()));
    connect(p_QueryCtrl,SIGNAL(NotifyEditRequest(QString)),p_Parent,SLOT(OnEditRequest(QString)));
}


int ManageInvoiceCtrl::GetCommandCount()
{
    return 1;
}

QAction *ManageInvoiceCtrl::GetAction(int iActionNo)
{
    switch (iActionNo) {
    case 0:
    {
        QAction* pAct=  new QAction(tr("Create Invoice"), this);
        pAct->setIcon(QIcon(":/image/Resources/receipt.png"));
        connect(pAct, SIGNAL(triggered(bool)),this, SLOT(OnCommandCreateInvoice()));
        return pAct;
        break;
    }
    default:
    {
        Q_ASSERT(false);
    }
        break;
    }
    return nullptr;
}

void ManageInvoiceCtrl::FixButton(int iActionNo, QPushButton *pButton)
{
    switch (iActionNo) {
    case 0:
    {
        pButton->setText(tr("Create Invoice"));
        pButton->setIcon(QIcon(":/image/Resources/receipt.png"));
        connect(pButton, SIGNAL(clicked(bool)),this, SLOT(OnCommandCreateInvoice()));
        break;
    }
    default:
    {
        Q_ASSERT(false);
    }
        break;
    }
}
