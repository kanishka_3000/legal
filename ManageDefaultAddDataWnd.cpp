/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "ManageDefaultAddDataWnd.h"

//#include <crmglobaldefs.h>
#include <ManageDefaultAddDataCtrl.h>
#include <QDebug>
#include <LGLDBFields.h>
ManageDefaultAddDataWnd::ManageDefaultAddDataWnd(MainWnd* pParent, QString sTitle) :
    GenericWnd(pParent, sTitle)
{
    setupUi(this);
    p_EditCtrl = NULL;
    p_Query->SetTableName(TBLNM_LGL_DEFAULTADDITIONALSTEPDATA);
    p_Query->SetKeyName(FDNM_LGL_DEFAULTADDITIONALSTEPDATA_DEFADDDATAID);
}

void ManageDefaultAddDataWnd::OnCreate()
{
    connect(p_Query,SIGNAL(NotifyNewRequest()),this, SLOT(OnAddService()));
    connect(p_Query,SIGNAL(NotifyEditRequest(QString)),this,SLOT(OnEditRequest(QString)));
     ReSize(400,400);
}

void ManageDefaultAddDataWnd::OnChildRemoveRequest(GenericCtrl *pChild)
{
    if(pChild == p_EditCtrl)
    {
        qDebug() << "Child removed";
        //delete p_EditCtrl;
        p_EditCtrl = NULL;
    }
}

ManageDefaultAddDataWnd::~ManageDefaultAddDataWnd()
{

}

void ManageDefaultAddDataWnd::OnAddService()
{

    tab_Ctrls->setCurrentIndex(1);
}

void ManageDefaultAddDataWnd::OnEditRequest(QString sKeyValue)
{
//    if(p_EditCtrl)
//    {
//        delete p_EditCtrl;
//    }
    std::shared_ptr<Entity> pEntity = Entity::FindInstance(sKeyValue,TBLNM_LGL_DEFAULTADDITIONALSTEPDATA);
    p_EditCtrl = new AddDefaultAddDataCtrl(this);
    OnNewChild(p_EditCtrl);
    int iTab = tab_Ctrls->addTab(p_EditCtrl,tr("Edit Group"));
    tab_Ctrls->setCurrentIndex(iTab);
    p_EditCtrl->PopulateWithEntity(pEntity);

}


