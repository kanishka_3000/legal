#include "AddAdditionalStepCtrl.h"
#include <LGLDBFields.h>
#include <GenericWnd.h>
#include <Util.h>
#include <LGLGlobalDefs.h>
#include <DefaultAddData.h>
AddAdditionalStepCtrl::AddAdditionalStepCtrl(QWidget *parent) :
   EntityManipulaterCtrl(parent)
{
    setupUi(this);
    b_DeleteWhenDone = true;
    SetEntityType(TBLNM_LGL_ADDITIONALSTEP);
    SetFieldProperty(txt_StepID, FDNM_LGL_ADDITIONALSTEP_STEPID);
    SetFieldProperty(txt_InvoiceID, FDNM_LGL_ADDITIONALSTEP_INVOICEID);
    SetFieldProperty(txt_MainText, FDNM_LGL_ADDITIONALSTEP_MAINTEXT);
    SetFieldProperty(spin_Price, FDNM_LGL_ADDITIONALSTEP_PRICE);
    SetFieldProperty(cmb_Stage, FDNM_LGL_ADDITIONALSTEP_STATE);
    SetEnumProperty(cmb_Stage, E_STATE);
    //SetFieldProperty(tx);

}

AddAdditionalStepCtrl::~AddAdditionalStepCtrl()
{

}

void AddAdditionalStepCtrl::OnCreateCtrl()
{
    btn_Close->setVisible(false);
    connect(btn_Save, SIGNAL(clicked(bool)), this, SLOT(OnSave()));
    connect(btn_Close, SIGNAL(clicked(bool)), this, SLOT(OnClose()));
    connect(cmb_DefItems, SIGNAL(activated(QString)), this, SLOT(OnDefItem(QString)));
    txt_StepID->setText(QString("%1").arg(Util::GetUnique()));
    FillComboWithEnum(cmb_Stage, E_STATE);
    cmb_DefItems->setModel(p_Parent->GetApplication()->GetTableStore()->GetTableModel(TBLNM_LGL_DEFAULTADDITIONALSTEPDATA));
    cmb_DefItems->setModelColumn(1);
    cmb_DefItems->addItem(SELECT_ITEM);
    cmb_DefItems->setCurrentText(SELECT_ITEM);
}

void AddAdditionalStepCtrl::PopulateWithEntity(std::shared_ptr<Entity> pEntity)
{
    EntityManipulaterCtrl::PopulateWithEntity(pEntity);
    btn_Close->setVisible(true);
}

void AddAdditionalStepCtrl::OnPreSave()
{
//    if(!b_EditMode)
//    {
//         p_Entity->ResetPrimaryKey();
//    }

}

void AddAdditionalStepCtrl::OnEditRequest(QString sAdditionalStepId)
{
    std::shared_ptr<Entity> pEntity = Entity::FindInstance(sAdditionalStepId,TBLNM_LGL_ADDITIONALSTEP);

    PopulateWithEntity(pEntity);
}

void AddAdditionalStepCtrl::OnClose()
{
    p_Parent->RemoveWindow();
    //deleteLater();
}

void AddAdditionalStepCtrl::OnDefItem(QString sData)
{
    QList<std::shared_ptr<Entity>> lstItems = Entity::FindInstances(FDNM_LGL_DEFAULTADDITIONALSTEPDATA_DISPLAYNAME, sData, TBLNM_LGL_DEFAULTADDITIONALSTEPDATA);
    foreach(std::shared_ptr<Entity> pEty, lstItems)
    {
        std::shared_ptr<DefaultAddData> pDefAddData = std::static_pointer_cast<DefaultAddData>(pEty);
        txt_MainText->setPlainText(pDefAddData->GetTitle());
        spin_Price->setValue(pDefAddData->GetPrice());
    }

}


void AddAdditionalStepCtrl::OnKey(QString sKey)
{
    txt_InvoiceID->setText(sKey);
}


void AddAdditionalStepCtrl::OnPostSave()
{

}


bool AddAdditionalStepCtrl::OnSave(EntityManipulaterSaveDislpayer *pSaveDisplayer)
{
    EntityManipulaterCtrl::OnSave(pSaveDisplayer);
    OnClose();
}
