#include "AddInvoiceDataCtrl.h"
#include <LGLDBFields.h>
#include <LGLGlobalDefs.h>
#include <GenericWnd.h>
#include <Util.h>
AddInvoiceDataCtrl::AddInvoiceDataCtrl(QWidget *parent) :
   EntityManipulaterCtrl(parent)
{
    setupUi(this);
    b_DeleteWhenDone = false;
    SetEntityType(TBLNM_LGL_DEFAULT_INVOICEDATA);
    SetFieldProperty(txt_DefaultText, FDNM_LGL_DEFAULT_INVOICEDATA_DEFAULT_TEXT);
    SetFieldProperty(spin_DefaultPrice, FDNM_LGL_DEFAULT_INVOICEDATA_DEFAULT_PRICE);
    SetFieldProperty(cmb_Court, FDNM_LGL_DEFAULT_INVOICEDATA_COURT_NAME);
    SetFieldProperty(cmb_InviiceType, FDNM_LGL_DEFAULT_INVOICEDATA_INVOICE_TYPE);
    SetFieldProperty(txt_ID, FDNM_LGL_DEFAULT_INVOICEDATA_INVOICEDATAID);
    SetEnumProperty(cmb_InviiceType, E_INVOICE_TYPE);

    SetFieldProperty(txt_Nextday, FDNM_LGL_DEFAULT_INVOICEDATA_DEFAULT_NEXTDAYTEXT);
    SetFieldProperty(txt_DefPriceText, FDNM_LGL_DEFAULT_INVOICEDATA_DEFAULT_MAINPRETEXT);
    //SetFieldProperty(tx);

}

AddInvoiceDataCtrl::~AddInvoiceDataCtrl()
{

}

void AddInvoiceDataCtrl::SetID()
{
    txt_ID->setText(QString("%1").arg(Util::GetUnique()));
}

void AddInvoiceDataCtrl::OnCreateCtrl()
{
    btn_Close->setVisible(false);
    SetID();
    FillComboWithEnum(cmb_InviiceType, E_INVOICE_TYPE);
    connect(btn_Save, SIGNAL(clicked(bool)), this, SLOT(OnSave()));
     connect(btn_Close, SIGNAL(clicked(bool)), this, SLOT(OnClose()));
    TableStore* pTableStore = p_Parent->GetApplication()->GetTableStore();
    cmb_Court->setModel(pTableStore->GetTableModel(TBLNM_LGL_COURT));
}

void AddInvoiceDataCtrl::PopulateWithEntity(std::shared_ptr<Entity> pEntity)
{
    EntityManipulaterCtrl::PopulateWithEntity(pEntity);
    btn_Close->setVisible(true);
}

void AddInvoiceDataCtrl::OnPreSave()
{
//    if(!b_EditMode)
//    {
//         p_Entity->ResetPrimaryKey();
//    }

}

void AddInvoiceDataCtrl::OnClose()
{
    p_Parent->OnChildRemoveRequest(this);
    deleteLater();
}


void AddInvoiceDataCtrl::OnPostSave()
{
    SetID();
    if(b_EditMode)
    {
        OnClose();
    }
}
