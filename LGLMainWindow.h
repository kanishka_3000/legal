/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <MainWnd.h>

class LGLMainWindow : public MainWnd
{
    Q_OBJECT

public:
    explicit LGLMainWindow(QWidget *parent = 0);

public slots:
    void OnCustomersWnd();
    void OnCaseWnd();
    void OnCourtWnd();
    void OnInvoiceDataWnd();
    void OnInvoiceWnd();
    void OnDefaultAddData();
    void OnReport();
};

#endif // MAINWINDOW_H
