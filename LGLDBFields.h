/***************************This is auto generated************************************
***************Kanishka Weerasekara (c)-- kkgweerasekara@gmail.com*******************
*************************************************************************************/
#ifndef __LGLDB_FIELDS__ 
#define __LGLDB_FIELDS__ 
//Table name for ROLE_PRIVILEDGE
#define TBLNM_LGL_ROLE_PRIVILEDGE	"Role_Priviledge" 
//Table fields for ROLE_PRIVILEDGE 
#define FDNM_LGL_ROLE_PRIVILEDGE_ROLENAME 	 "rolename"
#define FDNM_LGL_ROLE_PRIVILEDGE_PRICODE 	 "pricode"
//Table name for ROLE
#define TBLNM_LGL_ROLE	"Role" 
//Table fields for ROLE 
#define FDNM_LGL_ROLE_ROLENAME 	 "rolename"
#define FDNM_LGL_ROLE_ROLEDESCRIPTION 	 "roledescription"
//Table name for PRIVILEDGE
#define TBLNM_LGL_PRIVILEDGE	"Priviledge" 
//Table fields for PRIVILEDGE 
#define FDNM_LGL_PRIVILEDGE_PRICODE 	 "pricode"
#define FDNM_LGL_PRIVILEDGE_NAME 	 "name"
#define FDNM_LGL_PRIVILEDGE_PREVDESCRIPTION 	 "prevdescription"
//Table name for LOGIN
#define TBLNM_LGL_LOGIN	"Login" 
//Table fields for LOGIN 
#define FDNM_LGL_LOGIN_USERNAME 	 "username"
#define FDNM_LGL_LOGIN_PASSWORD 	 "password"
#define FDNM_LGL_LOGIN_NAME 	 "name"
#define FDNM_LGL_LOGIN_ROLENAME 	 "rolename"
//Table name for CUSTOMER
#define TBLNM_LGL_CUSTOMER	"customer" 
//Table fields for CUSTOMER 
#define FDNM_LGL_CUSTOMER_PLANTIFF_NAME 	 "plantiff_name"
#define FDNM_LGL_CUSTOMER_NAME 	 "name"
#define FDNM_LGL_CUSTOMER_MAIN_CONTACT_NAME 	 "main_contact_name"
#define FDNM_LGL_CUSTOMER_MAIN_CONTACT_NO 	 "main_contact_no"
#define FDNM_LGL_CUSTOMER_ADDRESS 	 "address"
#define FDNM_LGL_CUSTOMER_REGISTRATION_DATE 	 "registration_date"
//Table name for COURT
#define TBLNM_LGL_COURT	"court" 
//Table fields for COURT 
#define FDNM_LGL_COURT_NAME 	 "name"
#define FDNM_LGL_COURT_ADDRESS 	 "address"
#define FDNM_LGL_COURT_DISPLAY_NAME 	 "display_name"
//Table name for ADDITIONALSTEP
#define TBLNM_LGL_ADDITIONALSTEP	"AdditionalStep" 
//Table fields for ADDITIONALSTEP 
#define FDNM_LGL_ADDITIONALSTEP_STEPID 	 "stepid"
#define FDNM_LGL_ADDITIONALSTEP_INVOICEID 	 "invoiceid"
#define FDNM_LGL_ADDITIONALSTEP_MAINTEXT 	 "maintext"
#define FDNM_LGL_ADDITIONALSTEP_PRICE 	 "price"
#define FDNM_LGL_ADDITIONALSTEP_STATE 	 "state"
//Table name for DEFAULT_INVOICEDATA
#define TBLNM_LGL_DEFAULT_INVOICEDATA	"default_invoicedata" 
//Table fields for DEFAULT_INVOICEDATA 
#define FDNM_LGL_DEFAULT_INVOICEDATA_COURT_NAME 	 "court_name"
#define FDNM_LGL_DEFAULT_INVOICEDATA_INVOICE_TYPE 	 "invoice_type"
#define FDNM_LGL_DEFAULT_INVOICEDATA_DEFAULT_TEXT 	 "default_text"
#define FDNM_LGL_DEFAULT_INVOICEDATA_DEFAULT_PRICE 	 "default_price"
#define FDNM_LGL_DEFAULT_INVOICEDATA_INVOICEDATAID 	 "invoicedataid"
#define FDNM_LGL_DEFAULT_INVOICEDATA_DEFAULT_NEXTDAYTEXT 	 "default_nextdaytext"
#define FDNM_LGL_DEFAULT_INVOICEDATA_DEFAULT_MAINPRETEXT 	 "default_mainpretext"
//Table name for COURTCASE
#define TBLNM_LGL_COURTCASE	"courtcase" 
//Table fields for COURTCASE 
#define FDNM_LGL_COURTCASE_CUSTOMER_NAME 	 "customer_name"
#define FDNM_LGL_COURTCASE_CASE_NO 	 "case_no"
#define FDNM_LGL_COURTCASE_AC_NO 	 "ac_no"
#define FDNM_LGL_COURTCASE_DEFENDANT 	 "defendant"
#define FDNM_LGL_COURTCASE_REGISTRATION_DATE 	 "registration_date"
#define FDNM_LGL_COURTCASE_COURT 	 "court"
#define FDNM_LGL_COURTCASE_AGREEMENTNO 	 "agreementno"
#define FDNM_LGL_COURTCASE_CASETRACKID 	 "casetrackid"
#define FDNM_LGL_COURTCASE_STATE 	 "state"
#define FDNM_LGL_COURTCASE_RECEIVEDDATE 	 "receiveddate"
#define FDNM_LGL_COURTCASE_AMOUNT 	 "Amount"
#define FDNM_LGL_COURTCASE_CASEFILEDATE 	 "casefiledate"
#define FDNM_LGL_COURTCASE_COUNSELNAME 	 "counselname"
#define FDNM_LGL_COURTCASE_COUNSELSNAME 	 "counselsname"
#define FDNM_LGL_COURTCASE_COUNSELSBRIEFDATE 	 "counselsbriefdate"
#define FDNM_LGL_COURTCASE_INSTRUCTINGLAWYERSNAME 	 "instructinglawyersname"
//Table name for DEFAULTADDITIONALSTEPDATA
#define TBLNM_LGL_DEFAULTADDITIONALSTEPDATA	"DefaultAdditionalStepData" 
//Table fields for DEFAULTADDITIONALSTEPDATA 
#define FDNM_LGL_DEFAULTADDITIONALSTEPDATA_DEFADDDATAID 	 "defadddataid"
#define FDNM_LGL_DEFAULTADDITIONALSTEPDATA_DISPLAYNAME 	 "displayname"
#define FDNM_LGL_DEFAULTADDITIONALSTEPDATA_TITLE 	 "title"
#define FDNM_LGL_DEFAULTADDITIONALSTEPDATA_PRICE 	 "price"
#define FDNM_LGL_DEFAULTADDITIONALSTEPDATA_DEFAULTFORTYPE 	 "defaultfortype"
//Table name for INVOICE
#define TBLNM_LGL_INVOICE	"invoice" 
//Table fields for INVOICE 
#define FDNM_LGL_INVOICE_CASE_NO 	 "case_no"
#define FDNM_LGL_INVOICE_MAIN_TEXT 	 "main_text"
#define FDNM_LGL_INVOICE_PRICE 	 "price"
#define FDNM_LGL_INVOICE_DATE 	 "date"
#define FDNM_LGL_INVOICE_INVOICE_NO 	 "invoice_no"
#define FDNM_LGL_INVOICE_INVOICETYPE 	 "invoicetype"
#define FDNM_LGL_INVOICE_NEXTDATE 	 "nextdate"
#define FDNM_LGL_INVOICE_NEXTDAYTEXT 	 "nextdaytext"
#define FDNM_LGL_INVOICE_INVOICETRACKID 	 "invoicetrackid"
#define FDNM_LGL_INVOICE_STATE 	 "state"
#define FDNM_LGL_INVOICE_MAINPRETEXT 	 "mainpretext"
#define FDNM_LGL_INVOICE_NEXTDAYCOURTNO 	 "nextdaycourtno"
#define FDNM_LGL_INVOICE_NEXTDAYABBRIVIATIONTEXT 	 "nextdayabbriviationtext"
#define FDNM_LGL_INVOICE_ABBRIVIATIONTEXT 	 "abbriviationtext"
#endif //  __DB_FIELDS__ 
