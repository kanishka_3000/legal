#ifndef BASEINVOICEMODEL
#define BASEINVOICEMODEL
#include <reporting/ReportableModel.h>
#include <core/InvoiceReportData.h>
#include <Invoice.h>
class BaseInvoiceModel: public reporting::ReportableModel
{
public:
    virtual void PushData(std::shared_ptr<Invoice> pInvoice, QString sDate) = 0;
    virtual void Rearrange() = 0;
};

#endif // BASEINVOICEMODEL

