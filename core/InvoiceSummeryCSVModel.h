#ifndef INVOICESUMMERYCSVMODEL_H
#define INVOICESUMMERYCSVMODEL_H
#include <feature/CSVExporter.h>
#include <Invoice.h>
#include <QVector>
#include <QtAlgorithms>
#define MAIN_TEXT "Main Text"
#define PRICE_TEXT "Price Text"
#define STEPS_TEXT "Steps"
#define PRICE "Amount"
#define NEXT_DATE "NEXT DATE"
#define NEXT_DATE_ABR "NEXT DATE STEPS"
#define NEXT_DATE_COURT "Next Date Court"
#define AGREEMENT_NO "AGREEMENT NO"
#define CASE_No "CASE NO"
#define LAST_CALLING_DATE "LAST CALLING DATE"
#define LAST_CALL_ABBRIV "LAST CALLING STEPS"

class InvoiceSummeryCSVModel:public feature::CSVExporter::CSVDataSource
{
public:
    class InvoiceCSVDataRow:public feature::CSVExporter::CSVDataRow
    {
    public:
        friend InvoiceSummeryCSVModel;
        InvoiceCSVDataRow(std::shared_ptr<Invoice> pInvoice, QStringList slHeaders,QMap<QString, QString> &mapFieldMap);
        // CSVDataRow interface
    public:
        virtual QString GetData(int iColumn) override;
        std::shared_ptr<Invoice> p_Invoice;
    private:

        QStringList sl_Headers;
        QMap<QString, QString> map_FieldMaps;
    };

    InvoiceSummeryCSVModel(QStringList slHeaders, QMap<QString, QString> &mapFieldMap);

    // CSVDataSource interface
public:
    virtual QStringList GetHeader() const override;
    virtual int GetItemCount() const override;
    virtual std::shared_ptr<feature::CSVExporter::CSVDataRow> GetDataRow(int iRow) const override;

    void PushData(std::shared_ptr<Invoice> pInvoice);
    void Rearrange();
private:
    QStringList sl_Headers;
    QMap<QString, QString> map_FieldMaps;
    QVector<std::shared_ptr<InvoiceSummeryCSVModel::InvoiceCSVDataRow>> vec_Data;


};

#endif // INVOICESUMMERYCSVMODEL_H
