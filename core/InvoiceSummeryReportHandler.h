/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef INVOICESUMMERYREPORTHANDLER_H
#define INVOICESUMMERYREPORTHANDLER_H
#include <QString>
#include <reporting/ReportManager.h>
class InvoiceSummeryDataProvider
{
public:
    virtual bool SetCurrentRow(int iRow) const = 0;
    virtual int GetColumnCount() const = 0;
    virtual int GetRowCount() const = 0;
    virtual QString GetData(QString sColumn) const = 0;
};

class ReportSummeryItem: public reporting::ReportData
{
public:


    // ReportData interface
public:
    virtual QVariant GetReportDataItem(QString sColumn);

};

class InvoiceSummeryReportHandler : public QObject,
        public reporting::ReportDataProvider
{
    Q_OBJECT

public:
    enum class ReportType
    {
        CASE_UPDATE,
        CASE_STATUS
    };

    InvoiceSummeryReportHandler(reporting::ReportableModel* pModel,QSettings* pSettings, ReportType eReportType, QObject *parent);
    ~InvoiceSummeryReportHandler();
    void Init(QMap<QString,QString> mapAdditionalData);
private:
    QSettings* p_Settings;
    reporting::ReportableModel* p_Model;
    reporting::ReportManager* p_ReportManager;

    QMap<QString, QString> map_ExternalData;

    // ReportDataProvider interface
public:
    virtual QVariant ProvideReportData(QString sFieldName);
};

#endif // INVOICESUMMERYREPORTHANDLER_H
