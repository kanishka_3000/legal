/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "InvoiceReportHandler.h"
#include <globaldefs.h>
#include <Invoice.h>
#include <LGLDBFields.h>
#include <LGLGlobalDefs.h>
#include <AdditionalStep.h>
#include <feature/StringDataProcessor.h>
#include <feature/EntityStringListSourceAdapter.h>
InvoiceReportHandler::InvoiceReportHandler(QObject *parent) : QObject(parent)
{

	p_ReportManager = nullptr;
}

InvoiceReportHandler::~InvoiceReportHandler()
{
	delete p_ReportManager;
}

void InvoiceReportHandler::RegisterExternalFields()
{
    p_ReportManager->RegisterExternalField("casetitle");
    p_ReportManager->RegisterExternalField("acno");
    p_ReportManager->RegisterExternalField("parties");
    p_ReportManager->RegisterExternalField("date");
    p_ReportManager->RegisterExternalField("invoiceno");
    p_ReportManager->RegisterExternalField("agreementno");
    p_ReportManager->RegisterExternalField("totalamount");
    p_ReportManager->RegisterExternalField("footer");
    p_ReportManager->RegisterExternalField("nextdaytext");
    //p_ReportManager->RegisterExternalField("description");
}

void InvoiceReportHandler::Init(QSettings *pConfig, QString sInvoiceId)
{
    s_InvoiceNo = sInvoiceId;
    p_Settings = pConfig;
    if(s_InvoiceNo.isEmpty())
    {
        //Need to log
       return;
    }
    p_ReportManager = new reporting::ReportManager("Invoice", pConfig);
    p_ReportManager->RegisterConfigField(ORG_NAME);
    p_ReportManager->RegisterConfigField(ORG_ADDRESS);
    p_ReportManager->RegisterConfigField(ORG_CONTACT);
    p_ReportManager->RegisterConfigField(ORG_MOBILE);
    p_ReportManager->RegisterConfigField(ORG_FAX);
    p_ReportManager->RegisterConfigField(ORG_EMAIL);


    RegisterExternalFields();
    FillData();
    p_ReportManager->SetExternlaDataProvider(this);
    p_ReportManager->RegisterModel(this);

    p_ReportManager->Initialize();
}

void InvoiceReportHandler::FillData()
{
    std::shared_ptr<Invoice> pInvoice = Entity::FindInstaceEx<Invoice>(s_InvoiceNo, TBLNM_LGL_INVOICE);
    std::shared_ptr<Case> pCase = pInvoice->GetCase();
    std::shared_ptr<Customer> pCustomer = pCase->GetCustomer();
    std::shared_ptr<Court> pCourt = pCase->GetCourt();
    QString sCaseTitle = QString("%1 Case No :- %2").arg(pCourt->GetDisplayName()).arg(pCase->GetCaseNo());
    map_ExternlData["casetitle"] = sCaseTitle;
    map_ExternlData["invoicedate"] = pInvoice->GetDate();
    QString sParties = QString("%1 Vs. %2").arg(pCustomer->GetPlantiffName()).arg(pCase->GetDefendant());
    map_ExternlData["parties"] = sParties;


    map_ExternlData["acno"] = pCase->GetACNo();
    map_ExternlData["agreementno"] = pCase->GetAgreementNo();
    map_ExternlData["invoiceno"] = pInvoice->getInvoiceNo();
    if(p_Settings->value(EBL_FOOTER).toBool() == true)
    {
        map_ExternlData["footer"] = REPORT_FOOTER;
    }


    feature::StringDataProcessor oProcessor;
    oProcessor.Bind(feature::EntityStringListSourceAdapter(pInvoice));
    int iTotalAccount = 0;
    iTotalAccount += pInvoice->GetAmount().toInt();
    std::shared_ptr<InvoiceDataItem> pItem(new InvoiceDataItem);

    pItem->SetDate(pInvoice->GetDate());
    QString sPriceText = pInvoice->GetPreText();
    QString sDescription = pInvoice->GetDescription();
    sDescription = sPriceText +" \n \t "+ sDescription;
    oProcessor.ProcessString(sDescription);
    AppendPrependLine(sDescription);
    pItem->SetDescription(sDescription);
    QString sAmount = QString("%1.00").arg(pInvoice->GetAmount());
    if(sAmount == "0.00")
    {
        sAmount = "";
    }

    pItem->SetAmount(sAmount);
    vec_TableData.push_back(pItem);

    QList<std::shared_ptr<Entity>> lstAdditionalData = Entity::FindInstances(FDNM_LGL_ADDITIONALSTEP_INVOICEID,
                                                            pInvoice->GetInvoiceTrackID(), TBLNM_LGL_ADDITIONALSTEP);
    foreach(std::shared_ptr<Entity> pEntity, lstAdditionalData)
    {

        std::shared_ptr<InvoiceDataItem> pMainItem2(new InvoiceDataItem);
        std::shared_ptr<AdditionalStep> pAdditionalStep = std::static_pointer_cast<AdditionalStep>(pEntity);
        int iState = pAdditionalStep->GetState();
        if(iState == 1)
            continue;
        pMainItem2->SetDescription(pAdditionalStep->GetText());
        pMainItem2->SetAmount(QString("%1.00").arg(pAdditionalStep->GetPrice()));
        pMainItem2->SetDate(pInvoice->GetDate());
        iTotalAccount += pAdditionalStep->GetPrice().toInt();
        vec_TableData.push_back(pMainItem2);
    }



     sDescription = pInvoice->GetNextDayText();
     if(sDescription == NOT_APPLICABLE)
     {
         sDescription = "";
     }
     oProcessor.ProcessString(sDescription);

     map_ExternlData["totalamount"] = QString("%1.00").arg(iTotalAccount);
     map_ExternlData["date"] = QDate::currentDate().toString("yyyy-MM-dd");
     map_ExternlData["nextdaytext"] = sDescription;
}

void InvoiceReportHandler::AppendPrependLine(QString &sText)
{
    sText = QString("\t \n ") + sText + QString("\t \n ");
}



QVariant InvoiceReportHandler::ProvideReportData(QString sFieldName)
{
    return map_ExternlData[sFieldName];
}


std::shared_ptr<reporting::ReportData> InvoiceReportHandler::GetReportDataRow(int iRow)
{
    if(vec_TableData.size() < iRow)
    {
        Q_ASSERT(false);
    }
    return vec_TableData[iRow];
}

int InvoiceReportHandler::GetRowCount()
{
    //return 1;
    return vec_TableData.size();
}

QVariant InvoiceDataItem::GetReportDataItem(QString sColumn)
{
    return map_Data[sColumn];
}
void InvoiceDataItem::SetDate( QString value)
{
    map_Data.insert("invoicedate", value.trimmed());
}
void InvoiceDataItem::SetDescription( QString value)
{
    map_Data.insert("description", value.trimmed());
}
void InvoiceDataItem::SetAmount( QString value)
{
    map_Data.insert("amount", value.trimmed());
}





