#include "InvoiceSummeryCSVModel.h"
#include <LGLGlobalDefs.h>

bool InvoiceComparator(std::shared_ptr<InvoiceSummeryCSVModel::InvoiceCSVDataRow>& lhs,
                       std::shared_ptr<InvoiceSummeryCSVModel::InvoiceCSVDataRow>& rhs)
{
    return (lhs->p_Invoice->GetDate()) < (rhs->p_Invoice->GetDate());
}

InvoiceSummeryCSVModel::InvoiceSummeryCSVModel(QStringList slHeaders, QMap<QString, QString>& mapFieldMap):
    sl_Headers(slHeaders), map_FieldMaps(mapFieldMap)
{

}



QStringList InvoiceSummeryCSVModel::GetHeader() const
{
    return sl_Headers;
}

int InvoiceSummeryCSVModel::GetItemCount() const
{
    return vec_Data.size();
}

std::shared_ptr<feature::CSVExporter::CSVDataRow> InvoiceSummeryCSVModel::GetDataRow(int iRow) const
{
    if(iRow >= vec_Data.size())
    {
        return nullptr;
    }
    return vec_Data.at(iRow);
}

void InvoiceSummeryCSVModel::PushData(std::shared_ptr<Invoice> pInvoice)
{
    std::shared_ptr<InvoiceSummeryCSVModel::InvoiceCSVDataRow> pRow(new InvoiceCSVDataRow(pInvoice, sl_Headers, map_FieldMaps) );
    vec_Data.push_back(pRow);
}

void InvoiceSummeryCSVModel::Rearrange()
{

    qSort(vec_Data.begin(), vec_Data.end(),  InvoiceComparator);
}


InvoiceSummeryCSVModel::InvoiceCSVDataRow::InvoiceCSVDataRow(std::shared_ptr<Invoice> pInvoice, QStringList slHeaders,QMap<QString, QString> &mapFieldMap)
{
    p_Invoice = pInvoice;
    map_FieldMaps = mapFieldMap;
    sl_Headers = slHeaders;
}

QString InvoiceSummeryCSVModel::InvoiceCSVDataRow::GetData(int iColumn)
{

QString sColumn = sl_Headers.at(iColumn);
QString sInternalName = map_FieldMaps[sColumn];
QString sData;
std::shared_ptr<Case> pCase = p_Invoice->GetCase();
sData = pCase->GetData(sInternalName).toString();
if(!sData.isEmpty())
{
    if(sData == NOT_APPLICABLE)
        sData  = "";

    return sData;
}

sData = p_Invoice->GetData(sInternalName).toString();

if(sData == NOT_APPLICABLE)
    sData  = "";

return sData;


}
