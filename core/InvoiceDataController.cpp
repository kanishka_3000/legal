/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "InvoiceDataController.h"
#include <LGLDBFields.h>
#include <Util.h>
InvoiceDataController::InvoiceDataController()
{
   p_EntityFactory = nullptr;
}

void InvoiceDataController::Init(LGLEntityFactory *pEntityFactory)
{
 p_EntityFactory = pEntityFactory;
}

void InvoiceDataController::AddDefaultAdditionData(QString sInvoiceID, int iInvoiceType)
{

    Q_ASSERT(p_EntityFactory != nullptr);

    QList<std::shared_ptr<DefaultAddData>> lstDefaultAdditionalData;
    Entity::FindInstancesEx<DefaultAddData>(FDNM_LGL_DEFAULTADDITIONALSTEPDATA_DEFAULTFORTYPE,
                                            QString("%1").arg(iInvoiceType),
                                            TBLNM_LGL_DEFAULTADDITIONALSTEPDATA,
                                            lstDefaultAdditionalData);
    int iMakeUnique  = 0;
    foreach(std::shared_ptr<DefaultAddData> pDefData, lstDefaultAdditionalData)
    {
       std::shared_ptr<AdditionalStep> pAddStep = p_EntityFactory->CreateEntityEx<AdditionalStep>(
                   TBLNM_LGL_ADDITIONALSTEP);

       pAddStep->AddData(FDNM_LGL_ADDITIONALSTEP_INVOICEID, sInvoiceID);
       pAddStep->AddData(FDNM_LGL_ADDITIONALSTEP_STATE, 0);
       pAddStep->AddData(FDNM_LGL_ADDITIONALSTEP_STEPID, QString("%1").arg(Util::GetUnique() + (iMakeUnique++)));
       CopyData(pDefData, FDNM_LGL_DEFAULTADDITIONALSTEPDATA_TITLE, pAddStep, FDNM_LGL_ADDITIONALSTEP_MAINTEXT);
       CopyData(pDefData, FDNM_LGL_DEFAULTADDITIONALSTEPDATA_PRICE, pAddStep, FDNM_LGL_ADDITIONALSTEP_PRICE);
       pAddStep->Save();
    }
}

void InvoiceDataController::CopyData(std::shared_ptr<DefaultAddData> pInput, QString sInputField, std::shared_ptr<AdditionalStep> pOutput, QString sOutputField)
{
    QVariant oData = pInput->GetData(sInputField);
    pOutput->AddData(sOutputField, oData);

}


