/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef INVOICEDATACONTROLLER_H
#define INVOICEDATACONTROLLER_H

#include <Invoice.h>
#include <DefaultAddData.h>
#include <AddAdditionalStepCtrl.h>
#include <LGLApplication.h>
#include <LGLEntityFactory.h>
class InvoiceDataController
{
public:
    InvoiceDataController();
    void Init(LGLEntityFactory* pEntityFactory);
    void AddDefaultAdditionData(QString sInvoiceID, int iInvoiceType);

protected:
    void CopyData(std::shared_ptr<DefaultAddData> pInput, QString sInputField,
                  std::shared_ptr<AdditionalStep> pOutput, QString sOutputField);

private:
    LGLEntityFactory* p_EntityFactory;
};

#endif // INVOICEDATACONTROLLER_H
