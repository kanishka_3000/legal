#ifndef CASESTATEMODEL_H
#define CASESTATEMODEL_H

#include <reporting/ReportableModel.h>
#include <core/BaseInvoiceModel.h>
#include <Invoice.h>
class CaseStateModel: public BaseInvoiceModel
{
public:
    CaseStateModel();

    // ReportableModel interface
public:
    virtual std::shared_ptr<reporting::ReportData> GetReportDataRow(int iRow) override;
    virtual int GetRowCount() override;

    // BaseInvoiceModel interface
public:
    virtual void PushData(std::shared_ptr<Invoice> pInvoice, QString sDate) override;
    virtual void Rearrange() override;

private:
    QMap<QString, std::shared_ptr<Invoice> > map_Sorted;
    QMap<int, std::shared_ptr<reporting::ReportData>> map_Data;
};

#endif // CASESTATEMODEL_H
