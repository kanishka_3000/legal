#include "CaseStateModel.h"
#include <LGLDBFields.h>
CaseStateModel::CaseStateModel()
{

}

std::shared_ptr<reporting::ReportData> CaseStateModel::GetReportDataRow(int iRow)
{
    return map_Data[iRow];
}

int CaseStateModel::GetRowCount()
{
    return map_Data.size();
}


void CaseStateModel::PushData(std::shared_ptr<Invoice> pInvoice, QString sDate)
{
    QString sCaseNo =  pInvoice->GetCaseNo();
    if(!map_Sorted.contains(sCaseNo))
    {
        map_Sorted.insert(sCaseNo, pInvoice);
    }
    else
    {
        std::shared_ptr<Invoice> pPrevInvoice = map_Sorted[sCaseNo];
        int iPrevInvoiceNo = pPrevInvoice->getInvoiceNo().toInt();
        int iCurrentInvoiceNo = pInvoice->getInvoiceNo().toInt();
        if(iCurrentInvoiceNo > iPrevInvoiceNo)
        {
            map_Sorted.insert(sCaseNo, pInvoice);
        }
    }
}

void CaseStateModel::Rearrange()
{
    int i = 0;
    for(auto itr = map_Sorted.begin(); itr != map_Sorted.end(); itr++)
    {
         std::shared_ptr<Invoice> pInvoice = itr.value();
         std::shared_ptr<Case> pCase = pInvoice->GetCase();
         std::shared_ptr<InvoiceReportData> pRptData(new InvoiceReportData);
         pRptData->SetData("row", QString("%1").arg(i+1));
         pRptData->SetData(FDNM_LGL_INVOICE_NEXTDATE, pInvoice->GetNextDate());
         pRptData->SetData(FDNM_LGL_INVOICE_NEXTDAYABBRIVIATIONTEXT,
                           pInvoice->GetNextDayAbbriviated());
         pRptData->SetData(FDNM_LGL_COURTCASE_AC_NO, pCase->GetACNo());
         pRptData->SetData(FDNM_LGL_COURTCASE_AGREEMENTNO, pCase->GetAgreementNo());
         pRptData->SetData(FDNM_LGL_COURTCASE_CASE_NO, pCase->GetCaseNo());
         map_Data.insert(i++, pRptData);
    }
}
