
/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "InvoiceSummeryReportHandler.h"
#include <globaldefs.h>
#include <LGLGlobalDefs.h>
InvoiceSummeryReportHandler::InvoiceSummeryReportHandler(reporting::ReportableModel *pModel, QSettings *pSettings, ReportType eReportType, QObject* parent):
     QObject(parent),p_Settings(pSettings),p_Model(pModel)
{
    QString sReportFile;
    switch(eReportType)
    {
    case ReportType::CASE_UPDATE:
    {
        sReportFile = "InvoiceSummery";
        break;
    }
    case ReportType::CASE_STATUS:
    {
        sReportFile = "CaseSummery";
        break;
    }
    }
    p_ReportManager = new reporting::ReportManager(sReportFile, pSettings);
    p_ReportManager->RegisterConfigField(ORG_NAME);
    p_ReportManager->RegisterConfigField(ORG_ADDRESS);
    p_ReportManager->RegisterExternalField("footer");
    p_ReportManager->RegisterExternalField("daterange");
    p_ReportManager->RegisterExternalField("customer");
    p_ReportManager->RegisterExternalField("arrangedby");
    p_ReportManager->RegisterExternalField("recordcount");
}

InvoiceSummeryReportHandler::~InvoiceSummeryReportHandler()
{
    delete p_ReportManager;
    p_ReportManager = nullptr;
}

void InvoiceSummeryReportHandler::Init(QMap<QString, QString> mapAdditionalData)
{
    p_ReportManager->SetExternlaDataProvider(this);
    p_ReportManager->RegisterModel(p_Model);
    map_ExternalData = mapAdditionalData;
    map_ExternalData["footer"] = REPORT_FOOTER;
    map_ExternalData["recordcount"] = QString("%1").arg(p_Model->GetRowCount());
    p_ReportManager->Initialize();

}

QVariant InvoiceSummeryReportHandler::ProvideReportData(QString sFieldName)
{
    return map_ExternalData[sFieldName];
}



