/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef INVOICEREPORTHANDLER_H
#define INVOICEREPORTHANDLER_H

#include <QObject>
#include <reporting/ReportManager.h>
#include <QVector>
class InvoiceDataItem: public reporting::ReportData
{


    // ReportData interface
public:
    virtual QVariant GetReportDataItem(QString sColumn);

    void SetDate( QString value);

    void SetDescription( QString value);

    void SetAmount( QString value);



private:
    QMap<QString, QString> map_Data;

};

class InvoiceReportHandler : public QObject,
        public reporting::ReportDataProvider, public reporting::ReportableModel
{
    Q_OBJECT
public:
    explicit InvoiceReportHandler(QObject *parent = 0);
    virtual ~InvoiceReportHandler();
    void Init(QSettings* pConfig, QString sInvoiceId);
    void FillData();
signals:

public slots:

private:
    void AppendPrependLine(QString& sText);
    reporting::ReportManager* p_ReportManager;
    QMap<QString, QString> map_ExternlData;
    QString s_InvoiceNo;

    QVector<std::shared_ptr<InvoiceDataItem>> vec_TableData;
    QSettings* p_Settings;
    // ReportDataProvider interface
public:
    virtual QVariant ProvideReportData(QString sFieldName);
    void RegisterExternalFields();


    // ReportableModel interface
public:
    virtual std::shared_ptr<reporting::ReportData> GetReportDataRow(int iRow);
    virtual int GetRowCount();
};

#endif // INVOICEREPORTHANDLER_H
