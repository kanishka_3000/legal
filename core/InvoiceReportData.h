#ifndef INVOICEREPORTDATA_H
#define INVOICEREPORTDATA_H
#include <QVariant>
#include <reporting/ReportableModel.h>
class InvoiceSummeryModel;
class InvoiceReportData: public reporting::ReportData
{
    friend InvoiceSummeryModel;
    // ReportData interface
public:

    virtual QVariant GetReportDataItem(QString sColumn);
    void SetData(QString sKey, QString sValue);

private:
    QMap<QString, QString> map_Data;
};
#endif // INVOICEREPORTDATA_H
