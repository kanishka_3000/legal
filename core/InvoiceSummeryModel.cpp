/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "InvoiceSummeryModel.h"
#include <feature/StringDataProcessor.h>
#include <feature/EntityStringListSourceAdapter.h>
InvoiceSummeryModel::InvoiceSummeryModel(QList<QString> lstTextFields, bool bSteps, bool bPrice, bool bNextDayCourtNo):
    lst_TextFields(lstTextFields),b_Steps(bSteps),b_Price(bPrice),b_NextDayCourtNo(bNextDayCourtNo)
{

}

void InvoiceSummeryModel::PushData(std::shared_ptr<Invoice> pInvoice, QString sDate)
{
    QList<std::shared_ptr<Invoice>> &lstInv = map_Sorted[sDate];
    lstInv.push_back(pInvoice);
}

void InvoiceSummeryModel::Rearrange()
{

    int i = 0;
    for( QMap<QString, QList<std::shared_ptr<Invoice>>>::iterator itr = map_Sorted.begin(); itr != map_Sorted.end(); itr++)
    {
        QString sDate = itr.key();
        QList<std::shared_ptr<Invoice>>& lstInvoices = itr.value();

        foreach(std::shared_ptr<Invoice> pInvoice, lstInvoices)
        {

            feature::StringDataProcessor oProcessor;
            oProcessor.Bind(feature::EntityStringListSourceAdapter(pInvoice));
            QString sMidText;
            std::shared_ptr<InvoiceReportData> pRptData(new InvoiceReportData);
            QString sTextValue;
            sTextValue = sDate;
            pRptData->SetData(FDNM_LGL_INVOICE_DATE, sTextValue);

            sTextValue = pInvoice->GetData(FDNM_LGL_INVOICE_CASE_NO).toString();
            std::shared_ptr<Case> pCase = Entity::FindInstaceEx<Case>(sTextValue, TBLNM_LGL_COURTCASE);
            sTextValue = QString("%1 \n(%2)").arg(pCase->GetCaseNo()).arg(pCase->GetCourt()->GetDisplayName());
            if(b_NextDayCourtNo)
            {
                int iNextDayCourt =  pInvoice->GetNextDayCourtNo();
                if(iNextDayCourt != 0)
                {
                    sTextValue+= QString("- C.N: %1").arg(iNextDayCourt);
                }
            }
            pRptData->SetData(FDNM_LGL_INVOICE_CASE_NO, sTextValue);
            sTextValue = pCase->GetAgreementNo();
             pRptData->SetData(FDNM_LGL_COURTCASE_AGREEMENTNO, sTextValue);
            foreach(QString sTextField, lst_TextFields)
            {
                QString sTextValue;
                sTextValue = pInvoice->GetData(sTextField).toString();
                if(sTextValue  == "N/A")
                {
                    sTextValue = "";
                }
                oProcessor.ProcessString(sTextValue);

                sMidText += QString("\n");

                sMidText += sTextValue;
            }
            if(b_Price)
            {
                QString sPrice = pInvoice->GetAmount();
                sMidText += QString("( Rs. ") += sPrice += ".00 )";
            }
            if(b_Steps)
            {
                AppendSteps(pInvoice, sMidText);
            }
            pRptData->SetData(FIELD_TEXT, sMidText);
            map_Data.insert(i, pRptData);
            i++;
        }
    }
}



std::shared_ptr<reporting::ReportData> InvoiceSummeryModel::GetReportDataRow(int iRow)
{
    return map_Data.value(iRow);
}

int InvoiceSummeryModel::GetRowCount()
{
    return map_Data.size();
}

void InvoiceSummeryModel::AppendSteps(std::shared_ptr<Invoice> pInvoice, QString &sData)
{
    QList<std::shared_ptr<AdditionalStep>> lstAddStep = pInvoice->GetAdditionalSteps();
    int i = 1;
    foreach(std::shared_ptr<AdditionalStep> pStep, lstAddStep)
    {
        int iState = pStep->GetState();
        if(iState == 1)
            continue;

        sData += QString("\n\t %1. ").arg(i++);
        sData += pStep->GetText();
        if(b_Price)
        {
            QString sPrice = pStep->GetPrice();
            sData += QString("( Rs. ") += sPrice += ".00)";
        }
    }
}



