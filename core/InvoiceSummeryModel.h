/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef INVOICESUMMERYMODEL_H
#define INVOICESUMMERYMODEL_H
#include <reporting/ReportableModel.h>
#include <Invoice.h>
#include <LGLDBFields.h>
#include <core/InvoiceReportData.h>
#include <core/BaseInvoiceModel.h>
#define FIELD_DATE "ISMdate"
#define FIELD_CASENO "ISMcaseno"
#define FIELD_TEXT "ISMtext"


class InvoiceSummeryModel: public BaseInvoiceModel
{
public:
    InvoiceSummeryModel(QList<QString> lstTextFields, bool bSteps, bool bPrice, bool bNextDayCourtNo);
    void PushData(std::shared_ptr<Invoice> pInvoice, QString sDate);
    void Rearrange();
    // ReportableModel interface
public:
    virtual std::shared_ptr<reporting::ReportData> GetReportDataRow(int iRow);
    virtual int GetRowCount();
private:

    void AppendSteps(std::shared_ptr<Invoice> pInvoice, QString& sData);
     QMap<QString, QList<std::shared_ptr<Invoice>>> map_Sorted;
     QMap<int, std::shared_ptr<reporting::ReportData>> map_Data;

     QList<QString> lst_TextFields;
     bool b_Steps;
     bool b_Price;
     bool b_NextDayCourtNo;
};

#endif // INVOICESUMMERYMODEL_H
