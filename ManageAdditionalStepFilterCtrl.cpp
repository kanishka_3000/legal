/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "ManageAdditionalStepFilterCtrl.h"
#include <LGLDBFields.h>
ManageAdditionalStepFilterCtrl::ManageAdditionalStepFilterCtrl(QWidget *parent)
    :GenericFilterCtrl(parent)
{
    setupUi(this);
    SetFieldProperty(txt_InvoiceID, FDNM_LGL_ADDITIONALSTEP_INVOICEID );
}


void ManageAdditionalStepFilterCtrl::OnKey(QString sKey)
{
    txt_InvoiceID->setText(sKey);
            OnFilter();
}


void ManageAdditionalStepFilterCtrl::OnCreateCtrl()
{
}
