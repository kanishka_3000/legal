/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef MANAGEDEFAULTADDDATAWND_H
#define MANAGEDEFAULTADDDATAWND_H

#include <QWidget>
#include <ui_ManageDefaultAddDataWnd.h>
#include <GenericWnd.h>
#include <AddDefaultAddDataCtrl.h>
class ManageDefaultAddDataWnd : public GenericWnd, public Ui::ManageDefaultAddDataWnd
{
    Q_OBJECT

public:
    explicit ManageDefaultAddDataWnd(MainWnd* pParent, QString sTitle);
    virtual void OnCreate();
    virtual void OnChildRemoveRequest(GenericCtrl* pChild);
    ~ManageDefaultAddDataWnd();

public slots:
    void OnAddService();
    virtual void OnEditRequest(QString sKeyValue);
protected:

private:
    AddDefaultAddDataCtrl* p_EditCtrl;
};

#endif // MANAGESERVICESGROUPWND_H
