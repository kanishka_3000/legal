/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef MANAGESERVICESGROUPWND_H
#define MANAGESERVICESGROUPWND_H

#include <QWidget>
#include "ui_ManageInvoiceDataWnd.h"
#include <GenericWnd.h>
#include <AddInvoiceDataCtrl.h>
class ManageInvoiceDataWnd : public GenericWnd, public Ui::ManageInvoiceDataWnd
{
    Q_OBJECT

public:
    explicit ManageInvoiceDataWnd(MainWnd* pParent, QString sTitle);
    virtual void OnCreate();
    virtual void OnChildRemoveRequest(GenericCtrl* pChild);
    ~ManageInvoiceDataWnd();

public slots:
    void OnAddService();
    virtual void OnEditRequest(QString sKeyValue);
protected:

private:
    AddInvoiceDataCtrl* p_EditCtrl;
};

#endif // MANAGESERVICESGROUPWND_H
