/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef MANAGECOURTCTRL_H
#define MANAGECOURTCTRL_H

#include <QWidget>
#include "ui_ManageCourtCtrl.h"
#include <QueryCtrl.h>
class ManageCourtCtrl : public GenericCtrl, public Ui::ManageServicesGroupCtrl
{
    Q_OBJECT

public:
    explicit ManageCourtCtrl(QWidget* pParent);
    ~ManageCourtCtrl();

private:




};

#endif // MANAGESERVICESGROUPCTRL_H
