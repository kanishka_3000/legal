/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef MANAGEADDITIONALSTEPFILTERCTRL_H
#define MANAGEADDITIONALSTEPFILTERCTRL_H

#include "ui_ManageAdditionalStepFilterCtrl.h"
#include <GenericFilterCtrl.h>
class ManageAdditionalStepFilterCtrl : public GenericFilterCtrl, private Ui::ManageAdditionalStepFilterCtrl
{
    Q_OBJECT

public:
    explicit ManageAdditionalStepFilterCtrl(QWidget *parent = 0);

    // GenericCtrl interface
public:
    virtual void OnKey(QString sKey);

    // GenericCtrl interface
public:
    virtual void OnCreateCtrl();

    // GenericCtrl interface
};

#endif // MANAGEADDITIONALSTEPFILTERCTRL_H
