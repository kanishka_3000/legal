/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "SummeryWnd.h"

SummeryWnd::SummeryWnd(MainWnd *parent,QString sTitle ) :
    GenericWnd(parent, sTitle)
{
    setupUi(this);
}
