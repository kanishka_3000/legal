/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "LGLMainWindow.h"
#include <LGLApplication.h>
#include <LGLGlobalDefs.h>
LGLMainWindow::LGLMainWindow(QWidget *parent) :
    MainWnd(parent)
{
    connect(pAct_Customers, SIGNAL(triggered(bool)), this , SLOT(OnCustomersWnd()));
    connect(pActCase, SIGNAL(triggered(bool)), this , SLOT(OnCaseWnd()));
    connect(pAct_Court, SIGNAL(triggered(bool)), this , SLOT(OnCourtWnd()));
     connect(pAct_Invoice_Data, SIGNAL(triggered(bool)), this , SLOT(OnInvoiceDataWnd()));
     connect(pAct_Invoice, SIGNAL(triggered(bool)), this , SLOT(OnInvoiceWnd()));

     connect(pActAdditional_Steps, SIGNAL(triggered(bool)), this , SLOT(OnDefaultAddData()));
     connect(pAct_Report, SIGNAL(triggered(bool)),this, SLOT(OnReport()));
}

void LGLMainWindow::OnCustomersWnd()
{
    p_Application->CreateWnd(WND_CUSTOMERS);
}

void LGLMainWindow::OnCaseWnd()
{
    p_Application->CreateWnd(WND_CASE);
}

void LGLMainWindow::OnCourtWnd()
{
    p_Application->CreateWnd(WND_COURT);
}

void LGLMainWindow::OnInvoiceDataWnd()
{
    p_Application->CreateWnd(WND_INVOICE_DATA);
}

void LGLMainWindow::OnInvoiceWnd()
{
    p_Application->CreateWnd(WND_INVOICE);
}

void LGLMainWindow::OnDefaultAddData()
{
    p_Application->CreateWnd(WND_DEFADDDATA);
}

void LGLMainWindow::OnReport()
{
    p_Application->CreateWnd(WND_REPORT);
}
