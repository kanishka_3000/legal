/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "LGLEntityFactory.h"
#include <Customer.h>
#include <Case.h>
#include <Court.h>
#include <InvoiceData.h>
#include <Invoice.h>
#include <AdditionalStep.h>
#include <LGLDBFields.h>
#include <DefaultAddData.h>
LGLEntityFactory::LGLEntityFactory()
{
    map_EntityNames.insert(TBLNM_LGL_CUSTOMER, FDNM_LGL_CUSTOMER_PLANTIFF_NAME);
    map_EntityNames.insert(TBLNM_LGL_COURTCASE, FDNM_LGL_COURTCASE_CASETRACKID);
    map_EntityNames.insert(TBLNM_LGL_COURT, FDNM_LGL_COURT_NAME);
    map_EntityNames.insert(TBLNM_LGL_DEFAULT_INVOICEDATA, FDNM_LGL_DEFAULT_INVOICEDATA_INVOICEDATAID);
    map_EntityNames.insert(TBLNM_LGL_INVOICE, FDNM_LGL_INVOICE_INVOICETRACKID);
    map_EntityNames.insert(TBLNM_LGL_ADDITIONALSTEP, FDNM_LGL_ADDITIONALSTEP_STEPID);
    map_EntityNames.insert(TBLNM_LGL_DEFAULTADDITIONALSTEPDATA, FDNM_LGL_DEFAULTADDITIONALSTEPDATA_DEFADDDATAID);
}

void LGLEntityFactory::RegisterEntities()
{
    EntityFactory::RegisterEntities();
    SetHandler<Customer>(TBLNM_LGL_CUSTOMER);
    SetHandler<Case>(TBLNM_LGL_COURTCASE);
    SetHandler<Court>(TBLNM_LGL_COURT);
    SetHandler<InvoiceData>(TBLNM_LGL_DEFAULT_INVOICEDATA);
    SetHandler<Invoice>(TBLNM_LGL_INVOICE);
    SetHandler<AdditionalStep>(TBLNM_LGL_ADDITIONALSTEP);
    SetHandler<DefaultAddData>(TBLNM_LGL_DEFAULTADDITIONALSTEPDATA);
}

