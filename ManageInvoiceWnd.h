/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef MANAGEINVOICEWND_H
#define MANAGEINVOICEWND_H

#include <QWidget>
#include "ui_ManageInvoiceWnd.h"
#include <GenericWnd.h>
#include <AddInvoiceCtrl.h>
class ManageInvoiceWnd : public GenericWnd, public Ui::ManageInvoiceWnd
{
    Q_OBJECT

public:
    explicit ManageInvoiceWnd(MainWnd* pParent, QString sTitle);
    virtual void OnCreate();
    virtual void OnChildRemoveRequest(GenericCtrl* pChild);
    ~ManageInvoiceWnd();
    void SetOpenType(int iType);
public slots:
    void OnAddService();
    virtual void OnEditRequest(QString sKeyValue);
protected:

private:
    AddInvoiceCtrl* p_EditCtrl;
};

#endif // MANAGESERVICESGROUPWND_H
