/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "ManageCourtWnd.h"

//#include <crmglobaldefs.h>
#include <ManageCourtCtrl.h>
#include <QDebug>
#include <LGLDBFields.h>
ManageCourtWnd::ManageCourtWnd(MainWnd* pParent, QString sTitle) :
    GenericWnd(pParent, sTitle)
{
    setupUi(this);
    p_EditCtrl = NULL;
    p_Query->SetTableName(TBLNM_LGL_COURT);
    p_Query->SetKeyName(FDNM_LGL_COURT_NAME);
}

void ManageCourtWnd::OnCreate()
{
    connect(p_Query,SIGNAL(NotifyNewRequest()),this, SLOT(OnAddService()));
    connect(p_Query,SIGNAL(NotifyEditRequest(QString)),this,SLOT(OnEditRequest(QString)));
     ReSize(400,400);
}

void ManageCourtWnd::OnChildRemoveRequest(GenericCtrl *pChild)
{
    if(pChild == p_EditCtrl)
    {
        qDebug() << "Child removed";
        //delete p_EditCtrl;
        p_EditCtrl = NULL;
    }
}

ManageCourtWnd::~ManageCourtWnd()
{

}

void ManageCourtWnd::OnAddService()
{

    tab_Ctrls->setCurrentIndex(1);
}

void ManageCourtWnd::OnEditRequest(QString sKeyValue)
{
//    if(p_EditCtrl)
//    {
//        delete p_EditCtrl;
//    }
    std::shared_ptr<Entity> pEntity = Entity::FindInstance(sKeyValue,TBLNM_LGL_COURT);
    p_EditCtrl = new AddCourtCtrl(this);
    OnNewChild(p_EditCtrl);
    int iTab = tab_Ctrls->addTab(p_EditCtrl,tr("Edit Group"));
    tab_Ctrls->setCurrentIndex(iTab);
    p_EditCtrl->PopulateWithEntity(pEntity);

}


