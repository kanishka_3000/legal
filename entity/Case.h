/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef CASE_H
#define CASE_H

#include <Entity.h>
#include <Customer.h>
#include <Court.h>
class Case : public Entity
{
public:
    Case();

    std::shared_ptr<Customer> GetCustomer();
    std::shared_ptr<Court> GetCourt();
    QString GetDefendant();
    QString GetCaseNo();
    QString GetACNo();
    QString GetAgreementNo();
    QString GetCaseTrackID();

    int GetStatus();
private:
    std::shared_ptr<Customer> p_Customer;
    std::shared_ptr<Court> p_Court;
};

#endif // CASE_H
