/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef CUSTOMER_H
#define CUSTOMER_H
#include <Entity.h>

class Customer:public Entity
{
public:
    Customer();
    QString GetCustomerName();
    int GetContactNo();
    QString GetAddress();
    QString GetPlantiffName();
};

#endif // CUSTOMER_H
