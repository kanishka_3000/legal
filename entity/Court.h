/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef COURT_H
#define COURT_H
#include <Entity.h>

class Court : public Entity
{
public:
    Court();
    QString GetDisplayName();
    QString GetName();
};

#endif // COURT_H
