/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "AdditionalStep.h"
#include <LGLDBFields.h>
AdditionalStep::AdditionalStep()
{
    s_PersistatName = TBLNM_LGL_ADDITIONALSTEP;
    s_DefName = TBLNM_LGL_ADDITIONALSTEP;
    s_Key = FDNM_LGL_ADDITIONALSTEP_STEPID;
}

QString AdditionalStep::GetText()
{
    return GetData(FDNM_LGL_ADDITIONALSTEP_MAINTEXT).toString();
}

QString AdditionalStep::GetPrice()
{
    return GetData(FDNM_LGL_ADDITIONALSTEP_PRICE).toString();
}

int AdditionalStep::GetState()
{
    return GetData(FDNM_LGL_ADDITIONALSTEP_STATE).toInt();
}

