/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef ADDITIONALSTEP_H
#define ADDITIONALSTEP_H

#include <Entity.h>
class AdditionalStep : public Entity
{
public:
    AdditionalStep();
    QString GetText();
    QString GetPrice();
    int GetState();
};

#endif // ADDITIONALSTEP_H
