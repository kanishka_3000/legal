/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "DefaultAddData.h"

DefaultAddData::DefaultAddData()
{
    s_PersistatName = TBLNM_LGL_DEFAULTADDITIONALSTEPDATA;
    s_DefName = TBLNM_LGL_DEFAULTADDITIONALSTEPDATA;
    s_Key = FDNM_LGL_DEFAULTADDITIONALSTEPDATA_DEFADDDATAID;
}

QString DefaultAddData::GetTitle()
{
    return GetData(FDNM_LGL_DEFAULTADDITIONALSTEPDATA_TITLE).toString();
}


double DefaultAddData::GetPrice()
{
    return GetData(FDNM_LGL_DEFAULTADDITIONALSTEPDATA_PRICE).toDouble();
}

