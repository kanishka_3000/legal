/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "Court.h"
#include <LGLDBFields.h>
Court::Court()
{
    s_PersistatName = TBLNM_LGL_COURT;
    s_DefName = TBLNM_LGL_COURT;
    s_Key = FDNM_LGL_COURT_NAME;
}

QString Court::GetDisplayName()
{
    return GetData(FDNM_LGL_COURT_DISPLAY_NAME).toString();
}

QString Court::GetName()
{
    return GetData(FDNM_LGL_COURT_NAME).toString();
}

