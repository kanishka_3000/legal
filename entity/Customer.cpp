#include "Customer.h"
#include <LGLDBFields.h>
Customer::Customer()
{
    s_PersistatName = "Customer";
    s_DefName = TBLNM_LGL_CUSTOMER;
    s_Key = FDNM_LGL_CUSTOMER_PLANTIFF_NAME;
}

QString Customer::GetCustomerName()
{
    return map_Data.value(FDNM_LGL_CUSTOMER_MAIN_CONTACT_NAME).toString();
}

int Customer::GetContactNo()
{
    return map_Data.value(FDNM_LGL_CUSTOMER_MAIN_CONTACT_NO).toInt();
}

QString Customer::GetAddress()
{
    return GetData(FDNM_LGL_CUSTOMER_ADDRESS).toString();
}

QString Customer::GetPlantiffName()
{
    return GetData(FDNM_LGL_CUSTOMER_PLANTIFF_NAME).toString();
}

