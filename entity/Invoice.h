/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef INVOICE_H
#define INVOICE_H

#include <Entity.h>
#include <Case.h>
#include <AdditionalStep.h>

class Invoice : public Entity
{
public:

    Invoice();
    std::shared_ptr<Case> GetCase();
    QString GetCaseNo();
    QString GetDate();
    QString GetAmount();
    QString GetDescription();
    QString getInvoiceNo();
    QString GetNextDayText();
    QString GetNextDate();
    QString GetPreText();
    QString GetNextDayAbbriviated();
    int GetState();
    int GetInvoieType();
    int GetNextDayCourtNo();

    QString GetInvoiceTrackID();
    QList<std::shared_ptr<AdditionalStep> > GetAdditionalSteps();
    void SetAdditionalSteps(QList<std::shared_ptr<AdditionalStep> > lstAddtionSteps);
    void SetCase(std::shared_ptr<Case> pCase);
    void FillAdditionalSteps();
private:
    std::shared_ptr<Case> p_Case;
    QList<std::shared_ptr<AdditionalStep> > lst_Steps;
};

#endif // INVOICE_H
