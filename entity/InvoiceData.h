/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef INVOICEDATA_H
#define INVOICEDATA_H
#include <Entity.h>

class InvoiceData : public Entity
{
public:
    InvoiceData();
    QString GetInvoiceType();
    QString GetDefaultText();
    double GetDefaulptPrice();
    QString GetNextDayText();
    QString GetPriceText();
};

#endif // INVOICEDATA_H
