/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "InvoiceData.h"
#include <LGLDBFields.h>
InvoiceData::InvoiceData()
{
    s_PersistatName = TBLNM_LGL_DEFAULT_INVOICEDATA;
    s_DefName = TBLNM_LGL_DEFAULT_INVOICEDATA;
    s_Key = FDNM_LGL_DEFAULT_INVOICEDATA_INVOICEDATAID;
}

QString InvoiceData::GetInvoiceType()
{
    return GetData(FDNM_LGL_DEFAULT_INVOICEDATA_INVOICE_TYPE).toString();
}

QString InvoiceData::GetDefaultText()
{
    return GetData(FDNM_LGL_DEFAULT_INVOICEDATA_DEFAULT_TEXT).toString();
}

double InvoiceData::GetDefaulptPrice()
{
    return GetData(FDNM_LGL_DEFAULT_INVOICEDATA_DEFAULT_PRICE).toDouble();
}

QString InvoiceData::GetNextDayText()
{
    return GetData(FDNM_LGL_DEFAULT_INVOICEDATA_DEFAULT_NEXTDAYTEXT).toString();
}

QString InvoiceData::GetPriceText()
{
    return GetData(FDNM_LGL_DEFAULT_INVOICEDATA_DEFAULT_MAINPRETEXT).toString();
}

