/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "Invoice.h"
#include <LGLDBFields.h>
Invoice::Invoice()
{
    s_PersistatName = TBLNM_LGL_INVOICE;
    s_DefName = TBLNM_LGL_INVOICE;
    s_Key = FDNM_LGL_INVOICE_INVOICETRACKID;
    p_Case = nullptr;
}

std::shared_ptr<Case> Invoice::GetCase()
{
    if(p_Case == nullptr)
    {
        p_Case =Entity::FindInstaceEx<Case>(GetData(FDNM_LGL_INVOICE_CASE_NO).toString(), TBLNM_LGL_COURTCASE);
    }
    return p_Case;
}

QString Invoice::GetCaseNo()
{
    return GetData(FDNM_LGL_INVOICE_CASE_NO).toString();
}

QString Invoice::GetDate()
{
    return GetData(FDNM_LGL_INVOICE_DATE).toString();
}

QString Invoice::GetAmount()
{
    return GetData(FDNM_LGL_INVOICE_PRICE).toString();
}

QString Invoice::GetDescription()
{
    return GetData(FDNM_LGL_INVOICE_MAIN_TEXT).toString();
}

QString Invoice::getInvoiceNo()
{
    return GetData(FDNM_LGL_INVOICE_INVOICE_NO).toString();
}

QString Invoice::GetNextDayText()
{
    return GetData(FDNM_LGL_INVOICE_NEXTDAYTEXT).toString();
}

QString Invoice::GetNextDate()
{
    return GetData(FDNM_LGL_INVOICE_NEXTDATE).toString();
}

QString Invoice::GetPreText()
{
    return GetData(FDNM_LGL_INVOICE_MAINPRETEXT).toString();
}

QString Invoice::GetNextDayAbbriviated()
{
    return GetData(FDNM_LGL_INVOICE_NEXTDAYABBRIVIATIONTEXT).toString();
}

int Invoice::GetState()
{
    return GetData(FDNM_LGL_INVOICE_STATE).toInt();
}

int Invoice::GetInvoieType()
{
    return GetData(FDNM_LGL_INVOICE_INVOICETYPE).toInt();
}

int Invoice::GetNextDayCourtNo()
{
    return GetData(FDNM_LGL_INVOICE_NEXTDAYCOURTNO).toInt();
}

QString Invoice::GetInvoiceTrackID()
{
    return GetData(FDNM_LGL_INVOICE_INVOICETRACKID).toString();
}

QList<std::shared_ptr<AdditionalStep> > Invoice::GetAdditionalSteps()
{
    return lst_Steps;
}

void Invoice::SetAdditionalSteps(QList<std::shared_ptr<AdditionalStep> > lstAddtionSteps)
{
    lst_Steps.clear();
    lst_Steps  = lstAddtionSteps;
}

void Invoice::SetCase(std::shared_ptr<Case> pCase)
{
    p_Case = pCase;
}

void Invoice::FillAdditionalSteps()
{
    lst_Steps.clear();
    Entity::FindInstancesEx<AdditionalStep>(FDNM_LGL_ADDITIONALSTEP_INVOICEID, GetInvoiceTrackID(),
                                            TBLNM_LGL_ADDITIONALSTEP, lst_Steps);

}


