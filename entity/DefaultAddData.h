/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef DEFAULTADDDATA_H
#define DEFAULTADDDATA_H
#include <Entity.h>
#include <LGLDBFields.h>
class DefaultAddData : public Entity
{
public:
    DefaultAddData();
    QString GetTitle();

    double GetPrice();
};

#endif // DEFAULTADDDATA_H
