/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "Case.h"
#include <LGLDBFields.h>
Case::Case()
{
    s_PersistatName = TBLNM_LGL_COURTCASE;
    s_DefName = TBLNM_LGL_COURTCASE;
    s_Key = FDNM_LGL_COURTCASE_CASETRACKID;
    p_Customer = nullptr;
    p_Court = nullptr;
}

std::shared_ptr<Customer> Case::GetCustomer()
{
    if(p_Customer == nullptr)
    {
        p_Customer = Entity::FindInstaceEx<Customer>(GetData(FDNM_LGL_COURTCASE_CUSTOMER_NAME).toString(),TBLNM_LGL_CUSTOMER);
    }
    return p_Customer;
}

std::shared_ptr<Court> Case::GetCourt()
{
    if(p_Court == nullptr)
    {
         p_Court = Entity::FindInstaceEx<Court>(GetData(FDNM_LGL_COURTCASE_COURT).toString(),TBLNM_LGL_COURT);
    }
    return p_Court;
}

QString Case::GetDefendant()
{
    return GetData(FDNM_LGL_COURTCASE_DEFENDANT).toString();
}

QString Case::GetCaseNo()
{
    return GetData(FDNM_LGL_COURTCASE_CASE_NO).toString();
}

QString Case::GetACNo()
{
    return GetData(FDNM_LGL_COURTCASE_AC_NO).toString();
}

QString Case::GetAgreementNo()
{
    return GetData(FDNM_LGL_COURTCASE_AGREEMENTNO).toString();
}

QString Case::GetCaseTrackID()
{
    return GetData(FDNM_LGL_COURTCASE_CASETRACKID).toString();
}

int Case::GetStatus()
{
    return GetData(FDNM_LGL_COURTCASE_STATE).toInt();
}

