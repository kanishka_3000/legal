/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "ManageCaseFilterCtrl.h"
#include <LGLDBFields.h>
ManageCaseFilterCtrl::ManageCaseFilterCtrl(QWidget *parent):
    GenericFilterCtrl(parent)

{
    setupUi(this);
    SetFieldProperty(txt_Customer, FDNM_LGL_COURTCASE_CUSTOMER_NAME);
    SetFieldProperty(txt_CaseNo, FDNM_LGL_COURTCASE_CASE_NO);
    SetFieldProperty(txt_AcNo, FDNM_LGL_COURTCASE_AC_NO);
    SetFieldProperty(txt_Defendant, FDNM_LGL_COURTCASE_DEFENDANT);
    SetFieldProperty(txt_AgreementNo, FDNM_LGL_COURTCASE_AGREEMENTNO);
    SetFieldProperty(dt_From, FDNM_LGL_COURTCASE_CASEFILEDATE);
    SetAsFilterBuddy(dt_From, dt_To);
}


void ManageCaseFilterCtrl::OnCreateCtrl()
{
    SetApplyButton(btn_Apply);
    SetClearButton(btn_Clear);
    SetCurrentDateToAllFields();
}


void ManageCaseFilterCtrl::OnKey(QString sKey)
{
    txt_Customer->setText(sKey);
    OnFilter();
}
