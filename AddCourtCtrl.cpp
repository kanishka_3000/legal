#include "AddCourtCtrl.h"
#include <LGLDBFields.h>
#include <GenericWnd.h>
AddCourtCtrl::AddCourtCtrl(QWidget *parent) :
   EntityManipulaterCtrl(parent)
{
    setupUi(this);
    b_DeleteWhenDone = false;
    SetEntityType(TBLNM_LGL_COURT);
    SetFieldProperty(txt_Name, FDNM_LGL_COURT_NAME);
    SetFieldProperty(txt_Address, FDNM_LGL_COURT_ADDRESS);
    SetFieldProperty(txt_LongName, FDNM_LGL_COURT_DISPLAY_NAME);

    //SetFieldProperty(tx);
}

AddCourtCtrl::~AddCourtCtrl()
{

}

void AddCourtCtrl::OnCreateCtrl()
{
    btn_Close->setVisible(false);
    connect(btn_Save, SIGNAL(clicked(bool)), this, SLOT(OnSave()));
    connect(btn_Close, SIGNAL(clicked(bool)), this, SLOT(OnClose()));

}

void AddCourtCtrl::PopulateWithEntity(std::shared_ptr<Entity> pEntity)
{
    EntityManipulaterCtrl::PopulateWithEntity(pEntity);
    btn_Close->setVisible(true);
    txt_Name->setReadOnly(true);
}

void AddCourtCtrl::OnPreSave()
{
//    if(!b_EditMode)
//    {
//         p_Entity->ResetPrimaryKey();
//    }

}

void AddCourtCtrl::OnClose()
{
    p_Parent->OnChildRemoveRequest(this);
    deleteLater();
}


void AddCourtCtrl::OnPostSave()
{
    if(b_EditMode)
    {
        OnClose();
    }
}
