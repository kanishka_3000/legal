/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "ManageInvoiceDataWnd.h"

//#include <crmglobaldefs.h>
#include <ManageInvoiceDataCtrl.h>
#include <QDebug>
#include <LGLDBFields.h>
ManageInvoiceDataWnd::ManageInvoiceDataWnd(MainWnd* pParent, QString sTitle) :
    GenericWnd(pParent, sTitle)
{
    setupUi(this);
    p_EditCtrl = NULL;
    p_Query->SetTableName(TBLNM_LGL_DEFAULT_INVOICEDATA);
    p_Query->SetKeyName(FDNM_LGL_DEFAULT_INVOICEDATA_INVOICEDATAID);
}

void ManageInvoiceDataWnd::OnCreate()
{
    connect(p_Query,SIGNAL(NotifyNewRequest()),this, SLOT(OnAddService()));
    connect(p_Query,SIGNAL(NotifyEditRequest(QString)),this,SLOT(OnEditRequest(QString)));
     ReSize(400,400);
}

void ManageInvoiceDataWnd::OnChildRemoveRequest(GenericCtrl *pChild)
{
    if(pChild == p_EditCtrl)
    {
        qDebug() << "Child removed";
        //delete p_EditCtrl;
        p_EditCtrl = NULL;
    }
}

ManageInvoiceDataWnd::~ManageInvoiceDataWnd()
{

}

void ManageInvoiceDataWnd::OnAddService()
{

    tab_Ctrls->setCurrentIndex(1);
}

void ManageInvoiceDataWnd::OnEditRequest(QString sKeyValue)
{
//    if(p_EditCtrl)
//    {
//        delete p_EditCtrl;
//    }
    std::shared_ptr<Entity> pEntity = Entity::FindInstance(sKeyValue,TBLNM_LGL_DEFAULT_INVOICEDATA);
    p_EditCtrl = new AddInvoiceDataCtrl(this);
    OnNewChild(p_EditCtrl);
    int iTab = tab_Ctrls->addTab(p_EditCtrl,tr("Edit Group"));
    tab_Ctrls->setCurrentIndex(iTab);
    p_EditCtrl->PopulateWithEntity(pEntity);

}


