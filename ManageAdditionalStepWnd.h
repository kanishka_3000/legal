/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef MANAGEADDITIONALSTEPWND_H
#define MANAGEADDITIONALSTEPWND_H

#include <QWidget>
#include "ui_ManageAdditionalStepWnd.h"
#include <GenericWnd.h>
#include <AddAdditionalStepCtrl.h>
class ManageAdditionalStepWnd : public GenericWnd, public Ui::ManageAdditionalStepWnd
{
    Q_OBJECT

public:
    explicit ManageAdditionalStepWnd(MainWnd* pParent, QString sTitle);
    virtual void OnCreate();
    virtual void OnChildRemoveRequest(GenericCtrl* pChild);
    ~ManageAdditionalStepWnd();

public slots:
    void OnAddService();
    virtual void OnEditRequest(QString sKeyValue);
protected:

private:
    AddAdditionalStepCtrl* p_EditCtrl;
};

#endif // MANAGESERVICESGROUPWND_H
