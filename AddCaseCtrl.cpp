#include "AddCaseCtrl.h"
#include <LGLDBFields.h>
#include <GenericWnd.h>
#include <Util.h>
#include <LGLGlobalDefs.h>
AddCaseCtrl::AddCaseCtrl(QWidget *parent) :
   EntityManipulaterCtrl(parent)
{
    setupUi(this);
    b_DeleteWhenDone = false;
    SetEntityType(TBLNM_LGL_COURTCASE);
    SetFieldProperty(cmb_Customer, FDNM_LGL_COURTCASE_CUSTOMER_NAME);
    SetFieldProperty(txt_CaseNo, FDNM_LGL_COURTCASE_CASE_NO);
    SetFieldProperty(txt_AcNo, FDNM_LGL_COURTCASE_AC_NO);
    SetFieldProperty(txt_Defendant, FDNM_LGL_COURTCASE_DEFENDANT);
    SetFieldProperty(cmb_Court, FDNM_LGL_COURTCASE_COURT);
    SetFieldProperty(txt_Agreement, FDNM_LGL_COURTCASE_AGREEMENTNO);
    SetFieldProperty(txt_CaseTrackID , FDNM_LGL_COURTCASE_CASETRACKID);
    SetFieldProperty(cmb_Status, FDNM_LGL_COURTCASE_STATE);
    SetFieldProperty(spin_Amount, FDNM_LGL_COURTCASE_AMOUNT);
    SetFieldProperty(txt_Counsel, FDNM_LGL_COURTCASE_COUNSELSNAME);
    SetFieldProperty(txt_Lawyer, FDNM_LGL_COURTCASE_INSTRUCTINGLAWYERSNAME);
    SetFieldProperty(dt_ReceivedDate, FDNM_LGL_COURTCASE_RECEIVEDDATE);
    SetFieldProperty(dt_BriefDate, FDNM_LGL_COURTCASE_COUNSELSBRIEFDATE);
    SetFieldProperty(dt_FileDate, FDNM_LGL_COURTCASE_CASEFILEDATE);

    SetEnumProperty(cmb_Status, E_CASE_STATE);
    //SetFieldProperty(tx);
}

AddCaseCtrl::~AddCaseCtrl()
{

}

void AddCaseCtrl::Reset()
{
    txt_CaseTrackID->setText(QString("%1").arg(Util::GetUnique()));
    txt_CaseNo->setText(QString("%1").arg(Util::GetUnique()));
}

void AddCaseCtrl::OnCreateCtrl()
{
    btn_Close->setVisible(false);
    connect(btn_Save, SIGNAL(clicked(bool)), this, SLOT(OnSave()));
    connect(btn_Close, SIGNAL(clicked(bool)),this, SLOT(OnClose()));
    //SetComboCompleter(cmb_Court);
    cmb_Court->setModel(p_Parent->GetApplication()->GetTableStore()->GetTableModel(TBLNM_LGL_COURT));
    cmb_Customer->setModel(p_Parent->GetApplication()->GetTableStore()->GetTableModel(TBLNM_LGL_CUSTOMER));
    cmb_Customer->addItem("");
    cmb_Customer->setCurrentText("");
    Reset();
    SetCurrentDateToAllFields();
    FillComboWithEnum(cmb_Status, E_CASE_STATE);
    SubscribeForFeedback(PopulationFeedback::EMPTY, this);
}

void AddCaseCtrl::PopulateWithEntity(std::shared_ptr<Entity> pEntity)
{
    EntityManipulaterCtrl::PopulateWithEntity(pEntity);
    btn_Close->setVisible(true);
    //txt_CaseNo->setReadOnly(true);
}

void AddCaseCtrl::OnPreSave()
{
//    if(!b_EditMode)
//    {
//         p_Entity->ResetPrimaryKey();
//    }

}

void AddCaseCtrl::OnClose()
{
    p_Parent->OnChildRemoveRequest(this);
    deleteLater();
}


void AddCaseCtrl::OnKey(QString sKey)
{
    cmb_Customer->setCurrentIndex(cmb_Customer->findText(sKey));
}


void AddCaseCtrl::OnPostSave()
{
    Reset();
    if(b_EditMode)
    {
        OnClose();
    }
}


void AddCaseCtrl::OnPopulation(PopulationFeedback eType, QString sFieldName, QWidget *pWidget)
{
    if(sFieldName == FDNM_LGL_COURTCASE_COUNSELSBRIEFDATE)
    {
        chk_BriefedDate->setChecked(false);
        dt_BriefDate->setEnabled(false);
    }
}
