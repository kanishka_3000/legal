/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef ADDCOURTCTRL_H
#define ADDCOURTCTRL_H

#include <QWidget>
#include "ui_AddCourtCtrl.h"
#include <EntityManipulaterCtrl.h>
class AddCourtCtrl : public EntityManipulaterCtrl, public Ui::AddCourtCtrl
{
    Q_OBJECT

public:
    explicit AddCourtCtrl(QWidget *parent = 0);
    ~AddCourtCtrl();
    void OnCreateCtrl();
    virtual void PopulateWithEntity(std::shared_ptr<Entity> pEntity);
    virtual void OnPreSave();

public slots:
    void OnClose();
private:


    // EntityManipulaterCtrl interface
protected:
    virtual void OnPostSave() override;
};

#endif // ADDSERVICEGROUPCTRL_H
