/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef MANAGECOURTWND_H
#define MANAGECOURTWND_H

#include <QWidget>
#include "ui_ManageCourtWnd.h"
#include <GenericWnd.h>
#include <AddCourtCtrl.h>
class ManageCourtWnd : public GenericWnd, public Ui::ManageCourtWnd
{
    Q_OBJECT

public:
    explicit ManageCourtWnd(MainWnd* pParent, QString sTitle);
    virtual void OnCreate();
    virtual void OnChildRemoveRequest(GenericCtrl* pChild);
    ~ManageCourtWnd();

public slots:
    void OnAddService();
    virtual void OnEditRequest(QString sKeyValue);
protected:

private:
    AddCourtCtrl* p_EditCtrl;
};

#endif // MANAGESERVICESGROUPWND_H
