/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef MANAGECASECTRL_H
#define MANAGECASECTRL_H

#include <QWidget>
#include "ui_ManageCaseCtrl.h"
#include <QueryCtrl.h>
#include <ManageInvoiceWnd.h>
class ManageCaseCtrl : public GenericCtrl, public QueryCtrlCommandHandler,public Ui::ManageCaseCtrl
{
    Q_OBJECT

public:
    explicit ManageCaseCtrl();
    ~ManageCaseCtrl();

private:
public slots:
    void OnAddInvoice();
    void OnFindInvoices();


    // GenericCtrl interface
public:
    virtual void OnCreateCtrl();

    // QueryCtrlCommandHandler interface
public:
    virtual int GetCommandCount();
    virtual QAction *GetAction(int iActionNo);
    virtual void FixButton(int iActionNo, QPushButton *pButton);

protected:
    void CreateInvoiceWnd(QString sKey, ManageInvoiceWnd*& pWnd);
};

#endif // MANAGESERVICESGROUPCTRL_H
