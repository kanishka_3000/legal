/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "ManageInvoiceFilterCtrl.h"
#include <LGLDBFields.h>
#include <Case.h>
#include <GenericWnd.h>
ManageInvoiceFilterCtrl::ManageInvoiceFilterCtrl(QWidget *parent)
    :GenericFilterCtrl(parent)
{
    setupUi(this);
    SetFieldProperty(txt_TrackNo, FDNM_LGL_INVOICE_CASE_NO);
    SetFieldProperty(txt_InvoiceNo, FDNM_LGL_INVOICE_INVOICE_NO);
    SetFieldProperty(txt_InvoiceType, FDNM_LGL_INVOICE_INVOICETYPE);
    SetFieldProperty(dt_From, FDNM_LGL_INVOICE_DATE);
    SetAsFilterBuddy(dt_From, dt_To);
}

ManageInvoiceFilterCtrl::~ManageInvoiceFilterCtrl()
{
    delete p_FieldCompleter;
}


void ManageInvoiceFilterCtrl::OnCreateCtrl()
{
    SetApplyButton(btn_Apply);
    SetClearButton(btn_Clear);
    p_FieldCompleter = new FieldCompleter(p_Parent->GetApplication(), TBLNM_LGL_COURTCASE);
    p_FieldCompleter->SetSource(txt_CaseNo, FDNM_LGL_COURTCASE_CASE_NO);
    p_FieldCompleter->SetDestination(txt_TrackNo, FDNM_LGL_COURTCASE_CASETRACKID);
    SetCurrentDateToAllFields();
}


void ManageInvoiceFilterCtrl::OnKey(QString sKey)
{

    std::shared_ptr<Case> pCase = Entity::FindInstaceEx<Case>(sKey, TBLNM_LGL_COURTCASE);

    txt_TrackNo->setText(sKey);
    txt_CaseNo->setText(pCase->GetCaseNo());
    OnFilter();
}
