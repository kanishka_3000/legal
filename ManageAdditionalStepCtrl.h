/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef MANAGEADDITIONALSTEPCTRL_H
#define MANAGEADDITIONALSTEPCTRL_H

#include <QWidget>
#include "ui_ManageAdditionalStepCtrl.h"
#include <QueryCtrl.h>
class ManageAdditionalStepCtrl : public GenericCtrl, public Ui::ManageAdditionalStepCtrl
{
    Q_OBJECT

public:
    explicit ManageAdditionalStepCtrl(QWidget* parent = nullptr);
    ~ManageAdditionalStepCtrl();
    void OnKey(QString sInvoiceTrackID);
    void ReloadQuery();
private:
    QString s_InvoiceTrackID;
protected slots:
    void OnAddService();
    void OnEditRequest(QString sEditIt);
    // GenericCtrl interface
public:
    virtual void OnCreateCtrl();
    void ConnectFilter();

};

#endif // MANAGESERVICESGROUPCTRL_H
