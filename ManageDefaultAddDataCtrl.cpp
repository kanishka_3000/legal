/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "ManageDefaultAddDataCtrl.h"


ManageDefaultAddDataCtrl::ManageDefaultAddDataCtrl(QWidget *pParent) :
    GenericCtrl(pParent)
{
    setupUi(this);

}

ManageDefaultAddDataCtrl::~ManageDefaultAddDataCtrl()
{

}


