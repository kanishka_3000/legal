/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef ADDADDITIONALSTEPCTRL_H
#define ADDADDITIONALSTEPCTRL_H

#include <QWidget>
#include "ui_AddAdditionalStepCtrl.h"
#include <EntityManipulaterCtrl.h>
class AddAdditionalStepCtrl : public EntityManipulaterCtrl, public Ui::AddAdditionalStepCtrl
{
    Q_OBJECT

public:
    explicit AddAdditionalStepCtrl(QWidget *parent = 0);
    ~AddAdditionalStepCtrl();
    void OnCreateCtrl();
    virtual void PopulateWithEntity(std::shared_ptr<Entity> pEntity);
    virtual void OnPreSave();
    void OnEditRequest(QString sAdditionalStepId);
public slots:
    void OnClose();
    void OnDefItem(QString sData);
private:


    // GenericCtrl interface
public:
    virtual void OnKey(QString sKey);

    // EntityManipulaterCtrl interface
protected:
    virtual void OnPostSave();

    // EntityManipulaterCtrl interface
public slots:
    virtual bool OnSave(EntityManipulaterSaveDislpayer *pSaveDisplayer) override;
};

#endif // ADDSERVICEGROUPCTRL_H
