#include "AddInvoiceCtrl.h"
#include <LGLDBFields.h>
#include <GenericWnd.h>
#include <LGLGlobalDefs.h>
#include <QMessageBox>
#include <Case.h>
#include <InvoiceData.h>
#include <Util.h>
#include <ManageAdditionalStepWnd.h>
#include <Invoice.h>
#include <QTimer>
AddInvoiceCtrl::AddInvoiceCtrl(QWidget *parent) :
   EntityManipulaterCtrl(parent)
{
    setupUi(this);
    b_DeleteWhenDone = false;
    SetEntityType(TBLNM_LGL_INVOICE);

    SetFieldProperty(txt_CaseTrackID, FDNM_LGL_INVOICE_CASE_NO);
    SetFieldProperty(txt_InvoiceNo, FDNM_LGL_INVOICE_INVOICE_NO);
    SetFieldProperty(txt_MainText, FDNM_LGL_INVOICE_MAIN_TEXT);
    SetFieldProperty(spin_Price, FDNM_LGL_INVOICE_PRICE);
    SetFieldProperty(dt_Date, FDNM_LGL_INVOICE_DATE);
    SetFieldProperty(cmb_InvoiceType, FDNM_LGL_INVOICE_INVOICETYPE);
    SetEnumProperty(cmb_InvoiceType, E_INVOICE_TYPE);
    SetFieldProperty(dt_NextDate , FDNM_LGL_INVOICE_NEXTDATE);
    SetFieldProperty(txt_NextDate , FDNM_LGL_INVOICE_NEXTDAYTEXT);
    SetFieldProperty(txt_InvoiceID, FDNM_LGL_INVOICE_INVOICETRACKID);
    SetFieldProperty(cmb_State, FDNM_LGL_INVOICE_STATE);
    SetFieldProperty(txt_ChargesText, FDNM_LGL_INVOICE_MAINPRETEXT);
    SetFieldProperty(spin_NexdayCourt, FDNM_LGL_INVOICE_NEXTDAYCOURTNO);
    SetFieldProperty(txt_NextDayAbbriviation, FDNM_LGL_INVOICE_NEXTDAYABBRIVIATIONTEXT);
    SetFieldProperty(txt_Abbriviation, FDNM_LGL_INVOICE_ABBRIVIATIONTEXT);
    SetEnumProperty(cmb_State, E_STATE);
}

AddInvoiceCtrl::~AddInvoiceCtrl()
{

}

void AddInvoiceCtrl::SetInvoiceNo()
{
    s_InvoiceTrackID = QString("%1").arg(Util::GetUnique());
    txt_InvoiceNo->setText(s_InvoiceTrackID);
    txt_InvoiceID->setText(s_InvoiceTrackID);

    QTimer::singleShot(100, this, SLOT(OnInitialAddStepFilter()));
}

void AddInvoiceCtrl::SetCaseModel()
{
    cmb_CaseNo->setModel(p_Parent->GetApplication()->GetTableStore()->GetTableModel(TBLNM_LGL_COURTCASE));
}

void AddInvoiceCtrl::OnCreateCtrl()
{
    SetCurrentDateToAllFields();
    btn_Close->setVisible(false);
    connect(btn_Save, SIGNAL(clicked(bool)), this, SLOT(OnSave()));
    connect(btn_Close, SIGNAL(clicked(bool)), this, SLOT(OnClose()));
    connect(cmb_InvoiceType, SIGNAL(activated(int)),this, SLOT(OnInvoiceType(int)));
    connect(btn_DefAddcosts, SIGNAL(clicked(bool)),this, SLOT(onDefaultAdditionalCosts()));

    SetCaseModel();
    cmb_CaseNo->setModelColumn(1);
    FillComboWithEnum(cmb_InvoiceType, E_INVOICE_TYPE);

    SetInvoiceNo();
    cmb_InvoiceType->addItem("");
    cmb_InvoiceType->setCurrentText("");

    cmb_CaseNo->addItem("");
    cmb_CaseNo->setCurrentText("");
    connect(cmb_CaseNo, SIGNAL(currentTextChanged(QString)), this, SLOT(OnCaseSelected(QString)));

    FillComboWithEnum(cmb_State, E_STATE);
    SubscribeForFeedback(PopulationFeedback::EMPTY, this);

    o_InvoiceDataController.Init((LGLEntityFactory*)p_Parent->GetApplication()->GetEntityFactory());
}

void AddInvoiceCtrl::PopulateWithEntity(std::shared_ptr<Entity> pEntity)
{
    EntityManipulaterCtrl::PopulateWithEntity(pEntity);
    btn_Close->setVisible(true);
    cmb_CaseNo->setDisabled(true);

    std::shared_ptr<Invoice> pInvoice = std::static_pointer_cast<Invoice>(pEntity);
    std::shared_ptr<Case> pCase = pInvoice->GetCase();

    cmb_CaseNo->setCurrentIndex(cmb_CaseNo->findText(pCase->GetCaseNo()));
    //txt_InvoiceNo->setReadOnly(true);
    btn_DefAddcosts->setVisible(false);
    s_InvoiceTrackID = pInvoice->GetInvoiceTrackID();
    p_AddStepCtrl->OnKey(s_InvoiceTrackID);

}

void AddInvoiceCtrl::OnPreSave()
{
//    if(!b_EditMode)
//    {
//         p_Entity->ResetPrimaryKey();
//    }

}



void AddInvoiceCtrl::OnClose()
{
    p_Parent->OnChildRemoveRequest(this);
    deleteLater();
}

void AddInvoiceCtrl::OnInvoiceType(int iIndex)
{
    p_Logger->WriteLog("AddInvoiceCtrl::OnInvoiceType %d", iIndex);
    std::shared_ptr<Case> pCase = Entity::FindInstaceEx<Case>(txt_CaseTrackID->text(), TBLNM_LGL_COURTCASE);
    if(pCase == nullptr)
    {
        QMessageBox::information(this, "Invalid Case !!", "Invalid Case");
        return;
    }
    QString sInvoiceType = GetWidgetText(cmb_InvoiceType);
    if(sInvoiceType.isEmpty())
        return;

    p_Logger->WriteLog("AddInvoiceCtrl::OnInvoiceType invoice combo %s", qPrintable(sInvoiceType));

    QList<std::shared_ptr<Entity> > lstEntity = Entity::FindInstances(FDNM_LGL_DEFAULT_INVOICEDATA_COURT_NAME, pCase->GetCourt()->GetName(), TBLNM_LGL_DEFAULT_INVOICEDATA);
     p_Logger->WriteLog("AddInvoiceCtrl::OnInvoiceType invoice count %d", lstEntity.size());
    foreach(std::shared_ptr<Entity> pEntity, lstEntity)
    {
        std::shared_ptr<InvoiceData> pInvoiceData = std::static_pointer_cast<InvoiceData>(pEntity);
        if(pInvoiceData->GetInvoiceType() == sInvoiceType)
        {
            txt_MainText->setPlainText(pInvoiceData->GetDefaultText());
            spin_Price->setValue(pInvoiceData->GetDefaulptPrice());
            txt_NextDate->setPlainText(pInvoiceData->GetNextDayText());
            txt_ChargesText->setPlainText(pInvoiceData->GetPriceText());
            break;
        }
    }

}



void AddInvoiceCtrl::OnCaseSelected(QString sCase)
{
    if(sCase.isEmpty())
    {
        txt_CaseTrackID->setText("");
        return;
    }

    QList<std::shared_ptr<Case>> lstCases;
    Entity::FindInstancesEx<Case>(FDNM_LGL_COURTCASE_CASE_NO, sCase, TBLNM_LGL_COURTCASE,lstCases);

    if(lstCases.size() != 1)
    {
        QMessageBox::warning(this, "Data inconsistancy", "Single case for single case id ");
        return;
    }
    std::shared_ptr<Case> pCase = lstCases.first();
    txt_CaseTrackID->setText(pCase->GetCaseTrackID());
}

void AddInvoiceCtrl::onDefaultAdditionalCosts()
{
    QString sType = cmb_InvoiceType->currentText();
    if(sType == "NONE" || sType.isEmpty())
        return;

    QMessageBox::StandardButton ebutton = QMessageBox::question(this, tr("Warning"), QString(tr("This will populate your invoice with <br/> "
                                              "Costs for the type %1. <br/> Are you sure?")).arg(cmb_InvoiceType->currentText()));

    if(ebutton ==  QMessageBox::No)
        return;

    Enamuration* pEnum = p_Parent->GetApplication()->GetEnumHandler()->GetEnum(E_INVOICE_TYPE);

   int iType = pEnum->GetValue(sType);
   o_InvoiceDataController.AddDefaultAdditionData(s_InvoiceTrackID, iType);
   p_AddStepCtrl->ReloadQuery();
   btn_DefAddcosts->setEnabled(false);
}

void AddInvoiceCtrl::OnInitialAddStepFilter()
{
    if(b_EditMode)
        return;

    p_AddStepCtrl->OnKey(s_InvoiceTrackID);
}


void AddInvoiceCtrl::OnKey(QString sKey)
{
    std::shared_ptr<Case> pCase = Entity::FindInstaceEx<Case>(sKey, TBLNM_LGL_COURTCASE);
    int iIndex = cmb_CaseNo->findText(pCase->GetCaseNo());
    if(iIndex == -1)
    {
        //This is a newly added case, not in the model that was loaded oncreate
        SetCaseModel();
        iIndex = cmb_CaseNo->findText(pCase->GetCaseNo());
    }
    cmb_CaseNo->setCurrentIndex(iIndex);
    txt_CaseTrackID->setText(pCase->GetCaseTrackID());
}


void AddInvoiceCtrl::OnPostSave()
{
    SetInvoiceNo();
    if(b_EditMode)
    {
        OnClose();
    }
}


void AddInvoiceCtrl::OnPopulation(PopulationFeedback eType, QString sFieldName, QWidget *pWidget)
{
    if(sFieldName == FDNM_LGL_INVOICE_NEXTDATE)
    {
        chk_NextDate->setChecked(false);
        dt_NextDate->setEnabled(false);
    }
}
