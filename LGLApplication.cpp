/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "LGLApplication.h"
#include <LGLGlobalDefs.h>
#include <ManageCustomerWnd.h>
#include <ManageCaseWnd.h>
#include <ManageCourtWnd.h>
#include <ManageInvoiceDataWnd.h>
#include <ManageInvoiceWnd.h>
#include <ManageAdditionalStepWnd.h>
#include <ManageDefaultAddDataWnd.h>
#include <SummeryWnd.h>
#include <LGLDBFields.h>
#include <AddAdditionalStepWnd.h>
LGLApplication::LGLApplication(QObject *parent)
{
 (void)parent;
}



void LGLApplication::RegisterWindows()
{
    Application::RegisterWindows();
    SetWindowHandler<ManageCustomerWnd>(WND_CUSTOMERS,"Manage Customers", i_MaxWndInstances);
    SetWindowHandler<ManageCaseWnd>(WND_CASE,"Manage CASE", i_MaxWndInstances);
    SetWindowHandler<ManageCourtWnd>(WND_COURT,"Manage Court", i_MaxWndInstances);
    SetWindowHandler<ManageInvoiceDataWnd>(WND_INVOICE_DATA,"Manage Invoice Data", i_MaxWndInstances);
    SetWindowHandler<ManageInvoiceWnd>(WND_INVOICE,"Manage Invoice", i_MaxWndInstances);
    SetWindowHandler<ManageAdditionalStepWnd>(WND_ADDITIONAL_STEP,"Manage Additional", i_MaxWndInstances);
    SetWindowHandler<ManageDefaultAddDataWnd>(WND_DEFADDDATA, "Manage Def Add", i_MaxWndInstances);
    SetWindowHandler<SummeryWnd>(WND_REPORT, "Reports", i_MaxWndInstances);
    SetWindowHandler<AddAdditionalStepWnd>(WND_ADDADDITIONAL_STEP, "Add Additional Step", i_MaxWndInstances);
}


QString LGLApplication::GetApplicationName()
{
    return tr("Legal Doc");
}


void LGLApplication::FillCustomEnums(EnumHandler &rEnumHander)
{
    rEnumHander.AddEnumData(E_INVOICE_TYPE, "Proxy", 1);
    rEnumHander.AddEnumData(E_INVOICE_TYPE, "Appearance", 2);
    rEnumHander.AddEnumData(E_INVOICE_TYPE, "NONE", 3);

     rEnumHander.AddEnumData(E_CASE_STATE, "Active", 0);
     rEnumHander.AddEnumData(E_CASE_STATE, "Inactive", 1);
     rEnumHander.AddEnumData(E_CASE_STATE, "Withdrawal", 2);
     rEnumHander.AddEnumData(E_CASE_STATE, "Settle", 3);
     rEnumHander.AddEnumData(E_CASE_STATE, "Take Step", 4);
}


void LGLApplication::SetupUserDataTables()
{
    p_DataSetupManager->AddUserDataTable(TBLNM_LGL_CUSTOMER);
    p_DataSetupManager->AddUserDataTable(TBLNM_LGL_COURT);
    p_DataSetupManager->AddUserDataTable(TBLNM_LGL_DEFAULT_INVOICEDATA);
    p_DataSetupManager->AddUserDataTable(TBLNM_LGL_COURTCASE);
    p_DataSetupManager->AddUserDataTable(TBLNM_LGL_INVOICE);
    p_DataSetupManager->AddUserDataTable(TBLNM_LGL_ADDITIONALSTEP);
    p_DataSetupManager->AddUserDataTable(TBLNM_LGL_DEFAULTADDITIONALSTEPDATA);

}


QString LGLApplication::GetLoginIcon()
{
    return ":/image/Resources/scale.ico";
}
