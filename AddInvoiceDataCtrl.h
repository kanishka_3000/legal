/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef ADDINVOICEDATACTRL_H
#define ADDINVOICEDATACTRL_H

#include <QWidget>
#include "ui_AddInvoiceDataCtrl.h"
#include <EntityManipulaterCtrl.h>
class AddInvoiceDataCtrl : public EntityManipulaterCtrl, public Ui::AddInvoiceDataCtrl
{
    Q_OBJECT

public:
    explicit AddInvoiceDataCtrl(QWidget *parent = 0);
    ~AddInvoiceDataCtrl();
    void OnCreateCtrl();
    virtual void PopulateWithEntity(std::shared_ptr<Entity> pEntity);
    virtual void OnPreSave();
    void SetID();

public slots:
    void OnClose();
private:


    // EntityManipulaterCtrl interface
protected:
    virtual void OnPostSave() override;
};

#endif // ADDINVOICEDATACTRL_H
