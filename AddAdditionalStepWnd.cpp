#include "AddAdditionalStepWnd.h"

AddAdditionalStepWnd::AddAdditionalStepWnd(MainWnd *parent,QString sTitle) :
    GenericWnd(parent, sTitle)
{
    setupUi(this);
}

void AddAdditionalStepWnd::OnAdditionalItemEditRequest(QString sAdditionalID)
{
    p_Cltr->OnEditRequest(sAdditionalID);
}

void AddAdditionalStepWnd::RegisterForSaveNotification(QueryCtrl *pWidget)
{
    connect(p_Cltr, SIGNAL(NotifyItemSaved(bool)), pWidget, SLOT(Reload()));
}



