#ifndef ADDADDITIONALSTEPWND_H
#define ADDADDITIONALSTEPWND_H

#include "ui_AddAdditionalStepWnd.h"
#include <GenericWnd.h>
#include <QueryCtrl.h>
class AddAdditionalStepWnd : public GenericWnd, private Ui::AddAdditionalStepWnd
{
    Q_OBJECT

public:
    explicit AddAdditionalStepWnd(MainWnd *parent, QString sTitle);
    void OnAdditionalItemEditRequest(QString sAdditionalID);
    void RegisterForSaveNotification(QueryCtrl* pWidget);

};

#endif // ADDADDITIONALSTEPWND_H
