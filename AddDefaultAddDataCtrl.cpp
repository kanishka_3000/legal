#include "AddDefaultAddDataCtrl.h"
#include <LGLDBFields.h>
#include <GenericWnd.h>
#include <Util.h>
#include <LGLGlobalDefs.h>
AddDefaultAddDataCtrl::AddDefaultAddDataCtrl(QWidget *parent) :
   EntityManipulaterCtrl(parent)
{
    setupUi(this);
    b_DeleteWhenDone = false;
    SetEntityType(TBLNM_LGL_DEFAULTADDITIONALSTEPDATA);
    SetFieldProperty(txt_ID,FDNM_LGL_DEFAULTADDITIONALSTEPDATA_DEFADDDATAID  );
    SetFieldProperty(txt_DisplayName,FDNM_LGL_DEFAULTADDITIONALSTEPDATA_DISPLAYNAME  );
    SetFieldProperty(txt_Title,FDNM_LGL_DEFAULTADDITIONALSTEPDATA_TITLE  );
    SetFieldProperty(spin_Price,FDNM_LGL_DEFAULTADDITIONALSTEPDATA_PRICE  );
    SetFieldProperty(cmb_DefInvoiceType, FDNM_LGL_DEFAULTADDITIONALSTEPDATA_DEFAULTFORTYPE);
    SetEnumProperty(cmb_DefInvoiceType, E_INVOICE_TYPE);

}

AddDefaultAddDataCtrl::~AddDefaultAddDataCtrl()
{

}

void AddDefaultAddDataCtrl::OnCreateCtrl()
{
    btn_Close->setVisible(false);
    txt_ID->setText(QString("%1").arg(Util::GetUnique()));
    connect(btn_Save, SIGNAL(clicked(bool)), this, SLOT(OnSave()));
    connect(btn_Close, SIGNAL(clicked(bool)), this, SLOT(OnClose()));
    FillComboWithEnum(cmb_DefInvoiceType, E_INVOICE_TYPE);
    cmb_DefInvoiceType->setCurrentText("NONE");
}

void AddDefaultAddDataCtrl::PopulateWithEntity(std::shared_ptr<Entity> pEntity)
{
    EntityManipulaterCtrl::PopulateWithEntity(pEntity);
    btn_Close->setVisible(true);
}

void AddDefaultAddDataCtrl::OnPreSave()
{
//    if(!b_EditMode)
//    {
//         p_Entity->ResetPrimaryKey();
//    }

}

void AddDefaultAddDataCtrl::OnClose()
{
    p_Parent->OnChildRemoveRequest(this);
    deleteLater();
}


void AddDefaultAddDataCtrl::OnPostSave()
{
    if(b_EditMode)
    {
        OnClose();
    }
}
