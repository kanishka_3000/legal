#-------------------------------------------------
#
# Project created by QtCreator 2016-06-04T11:37:58
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
INCLUDEPATH += "../ApplicationBase"
DEPENDPATH += "../ApplicationBase"
INCLUDEPATH += $$PWD/entity
#RC_FILE = crm.rc
include(../ApplicationBase/ApplicationBase.pri)
TARGET = Legal
TEMPLATE = app

RC_FILE = legal.rc



SOURCES += $$PWD/LGLObjecFactory.cpp \
    $$PWD/LGLEntityFactory.cpp \
    $$PWD/LGLMainWindow.cpp \
    $$PWD/LGLApplication.cpp \
    $$PWD/entity/Customer.cpp \
    $$PWD/CustomerRegisterCtrl.cpp \
    $$PWD/ManageCustomerFilterCtrl.cpp \
    $$PWD/ManageCustomerWnd.cpp \
    $$PWD/entity/Case.cpp \
    $$PWD/ManageCaseCtrl.cpp \
    $$PWD/ManageCaseWnd.cpp \
    $$PWD/AddCaseCtrl.cpp \
    $$PWD/AddCourtCtrl.cpp \
    $$PWD/ManageCourtCtrl.cpp \
    $$PWD/ManageCourtWnd.cpp \
    $$PWD/entity/Court.cpp \
    $$PWD/AddInvoiceDataCtrl.cpp \
    $$PWD/ManageInvoiceDataCtrl.cpp \
    $$PWD/ManageInvoiceDataWnd.cpp \
    $$PWD/entity/InvoiceData.cpp \
    $$PWD/ManageCaseFilterCtrl.cpp \
    $$PWD/entity/Invoice.cpp \
    $$PWD/AddInvoiceCtrl.cpp \
    $$PWD/ManageInvoiceCtrl.cpp \
    $$PWD/ManageInvoiceFilterCtrl.cpp \
    $$PWD/ManageInvoiceWnd.cpp \
    $$PWD/core/InvoiceReportHandler.cpp \
    $$PWD/AddAdditionalStepCtrl.cpp \
    $$PWD/ManageAdditionalStepCtrl.cpp \
    $$PWD/ManageAdditionalStepFilterCtrl.cpp \
    $$PWD/ManageAdditionalStepWnd.cpp \
    $$PWD/entity/AdditionalStep.cpp \
    $$PWD/entity/DefaultAddData.cpp \
    $$PWD/AddDefaultAddDataCtrl.cpp \
    $$PWD/ManageDefaultAddDataCtrl.cpp \
    $$PWD/ManageDefaultAddDataWnd.cpp \
    $$PWD/SummeryWnd.cpp \
    $$PWD/SummeryCtrl.cpp \
    $$PWD/core/InvoiceSummeryReportHandler.cpp \
    $$PWD/core/InvoiceSummeryModel.cpp

HEADERS  += $$PWD/LGLObjecFactory.h \
    $$PWD/LGLEntityFactory.h \
    $$PWD/LGLMainWindow.h \
    $$PWD/LGLApplication.h \
    $$PWD/entity/Customer.h \
    $$PWD/LGLDBFields.h \
    $$PWD/CustomerRegisterCtrl.h \
    $$PWD/ManageCustomerFilterCtrl.h \
    $$PWD/ManageCustomerWnd.h \
    $$PWD/LGLGlobalDefs.h \
    $$PWD/entity/Case.h \
    $$PWD/ManageCaseCtrl.h \
    $$PWD/ManageCaseWnd.h \
    $$PWD/AddCaseCtrl.h \
    $$PWD/AddCourtCtrl.h \
    $$PWD/ManageCourtCtrl.h \
    $$PWD/ManageCourtWnd.h \
    $$PWD/entity/Court.h \
    $$PWD/AddInvoiceDataCtrl.h \
    $$PWD/ManageInvoiceDataCtrl.h \
    $$PWD/ManageInvoiceDataWnd.h \
    $$PWD/entity/InvoiceData.h \
    $$PWD/ManageCaseFilterCtrl.h \
    $$PWD/entity/Invoice.h \
    $$PWD/AddInvoiceCtrl.h \
    $$PWD/ManageInvoiceCtrl.h \
    $$PWD/ManageInvoiceFilterCtrl.h \
    $$PWD/ManageInvoiceWnd.h \
    $$PWD/core/InvoiceReportHandler.h \
    $$PWD/AddAdditionalStepCtrl.h \
    $$PWD/ManageAdditionalStepCtrl.h \
    $$PWD/ManageAdditionalStepFilterCtrl.h \
    $$PWD/ManageAdditionalStepWnd.h \
    $$PWD/entity/AdditionalStep.h \
    $$PWD/entity/DefaultAddData.h \
    $$PWD/AddDefaultAddDataCtrl.h \
    $$PWD/ManageDefaultAddDataCtrl.h \
    $$PWD/ManageDefaultAddDataWnd.h \
    $$PWD/SummeryWnd.h \
    $$PWD/SummeryCtrl.h \
    $$PWD/core/InvoiceSummeryReportHandler.h \
    $$PWD/core/InvoiceSummeryModel.h

FORMS    += $$PWD/MainWnd.ui \
    $$PWD/CustomerRegisterCtrl.ui \
    $$PWD/ManageCustomerFilterCtrl.ui \
    $$PWD/ManageCustomerWnd.ui \
    $$PWD/ManageCaseCtrl.ui \
    $$PWD/ManageCaseWnd.ui \
    $$PWD/AddCaseCtrl.ui \
    $$PWD/AddCourtCtrl.ui \
    $$PWD/ManageCourtCtrl.ui \
    $$PWD/ManageCourtWnd.ui \
    $$PWD/AddInvoiceDataCtrl.ui \
    $$PWD/ManageInvoiceDataCtrl.ui \
    $$PWD/ManageInvoiceDataWnd.ui \
    $$PWD/ManageServicesGroupWnd.ui \
    $$PWD/ManageCaseFilterCtrl.ui \
    $$PWD/AddInvoiceCtrl.ui \
    $$PWD/ManageInvoiceCtrl.ui \
    $$PWD/ManageInvoiceFilterCtrl.ui \
    $$PWD/ManageInvoiceWnd.ui \
    $$PWD/AddAdditionalStepCtrl.ui \
    $$PWD/ManageAdditionalStepCtrl.ui \
    $$PWD/ManageAdditionalStepFilterCtrl.ui \
    $$PWD/ManageAdditionalStepWnd.ui \
    $$PWD/AddDefaultAddDataCtrl.ui \
    $$PWD/ManageDefaultAddDataCtrl.ui \
    $$PWD/ManageDefaultAddDataWnd.ui \
    $$PWD/SummeryWnd.ui \
    $$PWD/SummeryCtrl.ui


RESOURCES += \
    $$PWD/Legal.qrc

DISTFILES += \
    Legal.pro.user
