/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef MANAGECASEFILTERCTRL_H
#define MANAGECASEFILTERCTRL_H

#include "ui_ManageCaseFilterCtrl.h"
#include <GenericFilterCtrl.h>
class ManageCaseFilterCtrl : public GenericFilterCtrl, private Ui::ManageCaseFilterCtrl
{
    Q_OBJECT

public:
    explicit ManageCaseFilterCtrl(QWidget *parent = 0);

    // GenericCtrl interface
public:
    virtual void OnCreateCtrl();

    // GenericCtrl interface
public:
    virtual void OnKey(QString sKey);
};

#endif // MANAGECASEFILTERCTRL_H
