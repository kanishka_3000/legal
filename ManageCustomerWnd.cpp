#include "ManageCustomerWnd.h"
//#include <crmglobaldefs.h>
#include <Customer.h>
//#include <CustomerRegisterWnd.h>
//#include <OrderWnd.h>
#include <QMessageBox>
//#include <OrderQueryWnd.h>
#include <LGLDBFields.h>

#include <LGLGlobalDefs.h>
ManageCustomerWnd::ManageCustomerWnd(MainWnd *parent, QString sTitle) :
GenericWnd(parent,sTitle)
{
    setupUi(this);
    p_Edit = NULL;
    p_QueryCtrl->SetTableName(TBLNM_LGL_CUSTOMER);
    p_QueryCtrl->SetKeyName(FDNM_LGL_CUSTOMER_PLANTIFF_NAME);
    p_QueryCtrl->SetCommandHanlder(this);
}

ManageCustomerWnd::~ManageCustomerWnd()
{

}

void ManageCustomerWnd::OnCreate()
{
    connect(p_QueryCtrl,SIGNAL(NotifyNewRequest()),this, SLOT(OnAddCustomer()));
    connect(p_FilterCtrl,SIGNAL(NotifyFilter(QList<FilterCrieteria>)),p_QueryCtrl,
            SLOT(OnFilter(QList<FilterCrieteria>)));
    connect(p_QueryCtrl,SIGNAL(NotifyEditRequest(QString)),this,SLOT(OnEditRequest(QString)));

    connect(pAct_New_Order, SIGNAL(triggered(bool)),this , SLOT(OnNewOrderCommand()));
    connect(pAct_Customer_s_Orders, SIGNAL(triggered(bool)),this, SLOT(OnCustomersOrdersCommand()));
    ReSize(700,400);
}

void ManageCustomerWnd::AdjustOpenType(ManageCustomerWnd::OpenType eOpenType)
{
    switch(eOpenType)
    {
     case OpenType::MANAGE:
    {
        p_Tab->setCurrentIndex(0);
        break;
    }
    case OpenType::NEW:
    {
        p_Tab->setCurrentIndex(1);
        break;
    }
    }
}

void ManageCustomerWnd::OnChildRemoveRequest(GenericCtrl *pChild)
{
    if(pChild == p_Edit)
    {
        p_Edit->deleteLater();
        p_Edit = NULL;
        p_Tab->setCurrentIndex(0);
    }
}

void ManageCustomerWnd::OnAddCustomer()
{
    p_Tab->setCurrentIndex(1);
    //p_Application->CreateWnd(WND_ADD_CUSTOMERS);
}

void ManageCustomerWnd::OnEditRequest(QString sKeyValue)
{
    if(p_Edit)
    {
        delete p_Edit;
    }
    //GenericWnd* pWnd = p_Application->CreateWnd(WND_ADD_CUSTOMERS);
    std::shared_ptr<Entity> pEntity = Entity::FindInstance(sKeyValue,TBLNM_LGL_CUSTOMER);
    std::shared_ptr<Customer> pCustomerr = std::dynamic_pointer_cast<Customer>(pEntity);

    p_Edit = new CustomerRegisterCtrl(this);
    OnNewChild(p_Edit);
    p_Edit->PopulateWithEntity(pCustomerr);
    p_Tab->addTab(p_Edit,tr("Update Customer"));
    p_Tab->setCurrentWidget(p_Edit);
    //CustomerRegisterWnd* pAddWnd = static_cast<CustomerRegisterWnd*>(pWnd);
    //pAddWnd->OpenAsEdit(pCustomerr);
}



void ManageCustomerWnd::OpenManageCase(QString sKey, ManageCaseWnd *&pWnd)
{
    pWnd = p_Application->CreateWndEx<ManageCaseWnd>(WND_CASE);
    pWnd->DistributeKeytoChildren(sKey);
}



void ManageCustomerWnd::OnNewOrderCommand()
{
    QString sKey = p_QueryCtrl->GetSelectedKey();
    if(sKey.isEmpty())
    {
        QMessageBox::warning(this, tr("Invalid Selection"), tr("Select a valid customer first"));
        return;
    }
    ManageCaseWnd* pWnd;
    OpenManageCase(sKey, pWnd);
    pWnd->SetOpenType(OPENTYPE_ADD);
}

void ManageCustomerWnd::OnCustomersOrdersCommand()
{
    QString sKey = p_QueryCtrl->GetSelectedKey();
    if(sKey.isEmpty())
    {
        QMessageBox::warning(this, tr("Invalid Selection"), tr("Select a valid customer first"));
        return;
    }
    ManageCaseWnd* pWnd;
    OpenManageCase(sKey, pWnd);
    pWnd->SetOpenType(OPENTYPE_MANAGE);

}

int ManageCustomerWnd::GetCommandCount()
{
    return 2;
}

QAction *ManageCustomerWnd::GetAction(int iActionNo)
{
    QAction* pAction = nullptr;
    switch(iActionNo)
    {
        case ACTION_NEW_ORDER:
        {
            pAction = pAct_New_Order;
            break;
        }
        case ACTION_ORDERS:
        {
            pAction = pAct_Customer_s_Orders;
            break;
        }
    }

    return pAction;
}

void ManageCustomerWnd::FixButton(int iActionNo, QPushButton *pButton)
{
    QAction* pAction = GetAction(iActionNo);

    switch(iActionNo)
    {
        case ACTION_NEW_ORDER:
        {
            connect(pButton, SIGNAL(clicked(bool)), this, SLOT(OnNewOrderCommand()));
            break;
        }
        case ACTION_ORDERS:
        {
            connect(pButton, SIGNAL(clicked(bool)), this, SLOT(OnCustomersOrdersCommand()));
            break;
        }
    }
    pButton->setIcon(pAction->icon());
    pButton->setText(pAction->text());

}

void ManageCustomerWnd::UpdateEditCommandName(QString &rEditButtnName)
{
    rEditButtnName = tr("Update Customer");
}

void ManageCustomerWnd::UpdateNewCommandName(QString &rNewCommandName)
{
    rNewCommandName = tr("Add Customer");
}
