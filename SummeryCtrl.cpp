/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "SummeryCtrl.h"
#include <GenericWnd.h>
#include <LGLDBFields.h>
#include <Util.h>
#include <core/InvoiceReportHandler.h>
#include <core/InvoiceSummeryModel.h>
#include <core/CaseStateModel.h>
#include <core/InvoiceSummeryCSVModel.h>
#include <QFileDialog>
SummeryCtrl::SummeryCtrl(QWidget *parent) :
    GenericCtrl(parent)
{
    setupUi(this);
    b_Initialized = false;

    AddCSVFieldMap(AGREEMENT_NO, FDNM_LGL_COURTCASE_AGREEMENTNO);
    AddCSVFieldMap(CASE_No, FDNM_LGL_COURTCASE_CASE_NO);
    AddCSVFieldMap(LAST_CALLING_DATE,FDNM_LGL_INVOICE_DATE );
    AddCSVFieldMap(LAST_CALL_ABBRIV, FDNM_LGL_INVOICE_ABBRIVIATIONTEXT);

    AddCSVFieldMap(MAIN_TEXT,FDNM_LGL_INVOICE_MAIN_TEXT );
    AddCSVFieldMap(PRICE_TEXT, FDNM_LGL_INVOICE_MAINPRETEXT);
    AddCSVFieldMap(PRICE, FDNM_LGL_INVOICE_PRICE);
    AddCSVFieldMap(NEXT_DATE, FDNM_LGL_INVOICE_NEXTDATE);
    AddCSVFieldMap(NEXT_DATE_ABR, FDNM_LGL_INVOICE_NEXTDAYABBRIVIATIONTEXT);
    AddCSVFieldMap(NEXT_DATE_COURT, FDNM_LGL_INVOICE_NEXTDAYCOURTNO);

}

void SummeryCtrl::OnCreateCtrl()
{
    connect(btn_Generate, SIGNAL(clicked(bool)), this, SLOT(OnGenerate()));
    //connect(cmb_Customer, SIGNAL(currentTextChanged(QString)), this, SLOT(OnShowCases(QString)));
    connect(btn_ChkAll, SIGNAL(clicked(bool)),this , SLOT(OnCheckAll()));
    connect(btn_UnckAll, SIGNAL(clicked(bool)),this, SLOT(OnUncheckAll()));

    connect(txt_QuickFind, SIGNAL(returnPressed()), this, SLOT(OnSearchCase()));
    connect(btn_SearchCases, SIGNAL(clicked(bool)),this, SLOT(OnFindUpdatedCases()));
    connect(btn_ExportToCSv, SIGNAL(clicked(bool)), this, SLOT(OnGenerateCSV()));

    SetCurrentDateToAllFields();
    cmb_Customer->setModel(p_Parent->GetApplication()->GetTableStore()->GetTableModel(TBLNM_LGL_CUSTOMER));
    cmb_Customer->addItem("");
    cmb_Customer->setCurrentText("");
}

void SummeryCtrl::ExtractFormInfo()
{
    s_CustomerName = cmb_Customer->currentText();

    s_StartDate = GetWidgetText(dt_IDStart);
    s_EndDate = GetWidgetText(dt_IDEnd);
    if(rdo_Invoice->isChecked())
    {
        s_DateField = FDNM_LGL_INVOICE_DATE;
    }
    else
    {
        s_DateField = FDNM_LGL_INVOICE_NEXTDATE;
    }
    set_CaseStates.clear();
    set_InvoiceTypes.clear();
    Util::CollectCheckState(chk_CSOpen, 0, set_CaseStates);
    Util::CollectCheckState(chk_CSWithdrwawal, 2, set_CaseStates);
    Util::CollectCheckState(chk_CSSettle, 3, set_CaseStates);
    Util::CollectCheckState(chk_CSTakeStep, 4, set_CaseStates);

    Util::CollectCheckState(chk_UTProxy, 1, set_InvoiceTypes);
    Util::CollectCheckState(chk_UTApperance, 2, set_InvoiceTypes);
    b_Initialized = true;
}

InvoiceSummeryReportHandler::ReportType SummeryCtrl::GetSelectedReportType()
{
    InvoiceSummeryReportHandler::ReportType eReportType;
    if(rdo_CaseStatus->isChecked())
    {
        eReportType = InvoiceSummeryReportHandler::ReportType::CASE_STATUS;
    }
    else
    {
        eReportType = InvoiceSummeryReportHandler::ReportType::CASE_UPDATE;
    }
    return eReportType;
}

void SummeryCtrl::CheckItems(bool bChecked)
{
    int iSize = list_Cases->count();
    for(int i = 0 ; i < iSize; i++)
    {
        QListWidgetItem* pItem = list_Cases->item(i);
        if(bChecked)
        {
            pItem->setCheckState(Qt::Checked);
        }
        else
        {
            pItem->setCheckState(Qt::Unchecked);
        }
    }
}

void SummeryCtrl::CollectFieldsIfChecked(QCheckBox *pCheckBox, QString sField, QList<QString> &lstFields)
{
    if(pCheckBox->isChecked())
    {
        lstFields << sField;
    }
}

void SummeryCtrl::AddCSVFieldMap(QString sFrom, QString sTo)
{
    map_CSVFieldMappings.insert(sFrom, sTo);
}

void SummeryCtrl::OnGenerate()
{
    ExtractFormInfo();

    QList<QString> lstTextFields;
    CollectFieldsIfChecked(chk_PriceText, FDNM_LGL_INVOICE_MAINPRETEXT, lstTextFields);
    CollectFieldsIfChecked(chk_MainText, FDNM_LGL_INVOICE_MAIN_TEXT, lstTextFields );
    CollectFieldsIfChecked(chk_NextDate, FDNM_LGL_INVOICE_NEXTDAYTEXT, lstTextFields);
    CollectFieldsIfChecked(chk_NDAbbrivatated, FDNM_LGL_INVOICE_NEXTDAYABBRIVIATIONTEXT, lstTextFields);


    bool bSteps = chk_Steps->isChecked();
    bool bPrice = chk_Price->isChecked();
    bool bNextDayCourt = chk_NDCourtNo->isChecked();

    BaseInvoiceModel* pModel = nullptr;
    InvoiceSummeryReportHandler::ReportType eReportType = GetSelectedReportType();
    switch(eReportType)
    {
    case InvoiceSummeryReportHandler::ReportType::CASE_UPDATE:
    {
      pModel =  new InvoiceSummeryModel(lstTextFields, bSteps, bPrice, bNextDayCourt);//need to delete
        break;
    }
    case InvoiceSummeryReportHandler::ReportType::CASE_STATUS:
    {
        pModel = new CaseStateModel();
        break;
    }
    }

    for(QMap<QString, QListWidgetItem*>::iterator itr = map_ListWidgetItems.begin();
        itr != map_ListWidgetItems.end(); itr++)
    {
        QListWidgetItem* pItem = itr.value();
        if(pItem->checkState() != Qt::Checked)
            continue;

        std::set<std::shared_ptr<Invoice>>& setInvoice = map_CaseUpdateByCase[itr.key()];

        for(auto itr = setInvoice.begin(); itr != setInvoice.end(); itr++)
        {
            std::shared_ptr<Invoice> pInvoice = *itr;
            int iInvoiceType = pInvoice->GetInvoieType();
            if(!set_InvoiceTypes.contains(iInvoiceType))
                    continue;

                    QString sDate;
                    if(rdo_Invoice->isChecked())
                    {
                        sDate = pInvoice->GetDate();
                    }
                    else
                    {
                        sDate = pInvoice->GetNextDate();
                    }
                    pInvoice->FillAdditionalSteps();
                    pModel->PushData(pInvoice, sDate);

        }
    }

    QMap<QString, QString> mapData;
    mapData.insert("daterange", QString("%1 - %2").arg(s_StartDate).arg(s_EndDate));
    mapData.insert("customer", s_CustomerName);
    mapData.insert("arrangedby", s_DateField.toUpper());
    pModel->Rearrange();

    InvoiceSummeryReportHandler oReportHandler(pModel,p_Parent->GetApplication()->GetConfig()
                                               ,eReportType, this);
    oReportHandler.Init(mapData);
}

void SummeryCtrl::OnGenerateCSV()
{
    ExtractFormInfo();

    QList<QString> lstTextFields;
    lstTextFields << AGREEMENT_NO;
    lstTextFields << CASE_No;
    lstTextFields << LAST_CALLING_DATE;
    CollectFieldsIfChecked(chk_Abbriviation, LAST_CALL_ABBRIV, lstTextFields);
    CollectFieldsIfChecked(chk_MainText, MAIN_TEXT, lstTextFields );
    CollectFieldsIfChecked(chk_NextDate, NEXT_DATE, lstTextFields);
    CollectFieldsIfChecked(chk_NDAbbrivatated, NEXT_DATE_ABR, lstTextFields);

    InvoiceSummeryCSVModel oModel(lstTextFields, map_CSVFieldMappings);
    for(QMap<QString, QListWidgetItem*>::iterator itr = map_ListWidgetItems.begin();
        itr != map_ListWidgetItems.end(); itr++)
    {
        QListWidgetItem* pItem = itr.value();
        if(pItem->checkState() != Qt::Checked)
            continue;

        std::set<std::shared_ptr<Invoice>>& setInvoice = map_CaseUpdateByCase[itr.key()];

        for(auto itr = setInvoice.begin(); itr != setInvoice.end(); itr++)
        {
            std::shared_ptr<Invoice> pInvoice = *itr;


            int iInvoiceType = pInvoice->GetInvoieType();
            if(!set_InvoiceTypes.contains(iInvoiceType))
                    continue;


                    pInvoice->FillAdditionalSteps();
                    oModel.PushData(pInvoice);

        }
    }
    feature::CSVExporter oExport;
    oExport.Init(&oModel);
    QString sHome = QDir::homePath();
    QString sPath = QFileDialog::getExistingDirectory(this, "Save Export", sHome);

    oModel.Rearrange();

    oExport.Export(sPath, QString("LE%1").arg(Util::GetUnique()));
}

void SummeryCtrl::OnCheckAll()
{
    CheckItems(true);
}

void SummeryCtrl::OnUncheckAll()
{
    CheckItems(false);
}

void SummeryCtrl::OnSearchCase()
{

    QString sSearchText = txt_QuickFind->text();

    QList<QListWidgetItem*> lstMatchingItems = list_Cases->findItems(sSearchText, Qt::MatchContains);
    if(lstMatchingItems.count() == 0)
        return;

    int iRow = list_Cases->row(lstMatchingItems.first());
    list_Cases->setCurrentRow(iRow);
    foreach(QListWidgetItem* pItem , lstMatchingItems)
    {
        pItem->setSelected(true);
    }


}

void SummeryCtrl::OnFindUpdatedCases()
{
    ExtractFormInfo();
    map_CaseUpdateByCase.clear();
    map_ListWidgetItems.clear();
    list_Cases->clear();
    QList<std::shared_ptr<Invoice>> lstInvoices;
    Entity::FindInstancesEx<Invoice>(s_DateField, s_StartDate, s_EndDate, TBLNM_LGL_INVOICE, lstInvoices);
    QSet<QString> setCaseIDs;
    foreach(std::shared_ptr<Invoice> pInvoice , lstInvoices)
    {
        std::shared_ptr<Case> pCase = pInvoice->GetCase();

        int iInvoiceState = pInvoice->GetState();
        if(iInvoiceState == 1)//Inactive case update
            continue;

        if(pCase->GetCustomer()->GetPlantiffName() != s_CustomerName)
            continue;

        int iCaseState = pCase->GetStatus();
        if(!set_CaseStates.contains(iCaseState))
            continue;
        std::set<std::shared_ptr<Invoice>>& setInvoice = map_CaseUpdateByCase[pCase->GetCaseNo()];
        setInvoice.insert(pInvoice);
        setCaseIDs.insert(pInvoice->GetCase()->GetCaseNo());//As a small optimization of getting keys from map
    }
    //Can do this using first loop
    foreach(QString sCaseID, setCaseIDs)
    {
        QListWidgetItem* pItem = new QListWidgetItem(sCaseID);
        map_ListWidgetItems.insert(sCaseID,(pItem));
        pItem->setFlags(pItem->flags() | Qt::ItemIsUserCheckable);
        pItem->setCheckState(Qt::Checked);
        list_Cases->addItem(pItem);
    }
}


