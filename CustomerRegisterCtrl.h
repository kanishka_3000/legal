/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef CUSTOMERREGISTERCTRL_H
#define CUSTOMERREGISTERCTRL_H

#include <QWidget>
#include <EntityManipulaterCtrl.h>
#include <ui_CustomerRegisterCtrl.h>

class CustomerRegisterCtrl : public EntityManipulaterCtrl,public Ui::CustomerRegisterCtrl
{
    Q_OBJECT

public:
    class CustomerRegisterCallback: public EntityManipulaterSaveDislpayer
    {
    public:
        CustomerRegisterCallback(QLabel* pLabel, CustomerRegisterCtrl* pParent):
            p_Label(pLabel),p_Parent(pParent){}
        void EntitySaved(bool bEdit) override;
    private:
        QLabel* p_Label;
        CustomerRegisterCtrl* p_Parent;
    };

    explicit CustomerRegisterCtrl(QWidget *parent = 0);
    ~CustomerRegisterCtrl();
    virtual void OnCreateCtrl();
    virtual void PopulateWithEntity(std::shared_ptr<Entity> pEntity);
protected:
    virtual bool CustomValidations(QString& sError);
public slots:
    void OnClose();
    bool OnSave(EntityManipulaterSaveDislpayer *pSaveDisplayer = nullptr);
private:


    // EntityManipulaterCtrl interface
protected:
    virtual void OnPostSave() override;
};

#endif // CUSTOMERREGISTERCTRL_H
