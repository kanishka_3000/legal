/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "ManageCaseWnd.h"

//#include <crmglobaldefs.h>
#include <LGLGlobalDefs.h>
#include <ManageCaseCtrl.h>
#include <QDebug>
#include <LGLDBFields.h>
ManageCaseWnd::ManageCaseWnd(MainWnd* pParent, QString sTitle) :
    GenericWnd(pParent, sTitle)
{
    setupUi(this);
    p_EditCtrl = NULL;

}

void ManageCaseWnd::OnCreate()
{

     ReSize(750,400);
}

void ManageCaseWnd::OnChildRemoveRequest(GenericCtrl *pChild)
{
    if(pChild == p_EditCtrl)
    {
        qDebug() << "Child removed";
        //delete p_EditCtrl;
        p_EditCtrl = NULL;
    }
}

ManageCaseWnd::~ManageCaseWnd()
{

}

void ManageCaseWnd::SetOpenType(int iType)
{
    switch(iType)
    {
    case OPENTYPE_ADD:
    {
        tab_Ctrls->setCurrentIndex(1);
        break;
    }
    case OPENTYPE_MANAGE:
    {
        tab_Ctrls->setCurrentIndex(0);
        break;
    }
    }
}

void ManageCaseWnd::OnAddService()
{

    tab_Ctrls->setCurrentIndex(1);
}

void ManageCaseWnd::OnEditRequest(QString sKeyValue)
{
//    if(p_EditCtrl)
//    {
//        delete p_EditCtrl;
//    }
    std::shared_ptr<Entity> pEntity = Entity::FindInstance(sKeyValue,TBLNM_LGL_COURTCASE);
    p_EditCtrl = new AddCaseCtrl(this);
    OnNewChild(p_EditCtrl);
    int iTab = tab_Ctrls->addTab(p_EditCtrl,tr("Edit Group"));
    tab_Ctrls->setCurrentIndex(iTab);
    p_EditCtrl->PopulateWithEntity(pEntity);

}


