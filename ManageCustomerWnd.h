/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef MANAGECUSTOMERWND_H
#define MANAGECUSTOMERWND_H

#include <QWidget>
#include "ui_ManageCustomerWnd.h"
#include <GenericWnd.h>
#include <CustomerRegisterCtrl.h>
#include <ManageCaseWnd.h>
#define ACTION_NEW_ORDER 0
#define ACTION_ORDERS 1
class ManageCustomerWnd : public GenericWnd, public QueryCtrlCommandHandler, public Ui::ManageCustomerWnd
{
    Q_OBJECT

public:
    enum class OpenType
    {
        MANAGE,
        NEW
    };

    explicit ManageCustomerWnd(MainWnd *parent,QString sTitle );
    ~ManageCustomerWnd();
    virtual void OnCreate();
    void AdjustOpenType(OpenType eOpenType);
protected:
    void OnChildRemoveRequest(GenericCtrl *pChild);

    void OpenManageCase(QString sKey, ManageCaseWnd*& pWnd);
public slots:

    void OnAddCustomer();
    virtual void OnEditRequest(QString sKeyValue);

    virtual void OnNewOrderCommand();
    virtual void OnCustomersOrdersCommand();
public:
    CustomerRegisterCtrl* p_Edit;


    // QueryCtrlCommandHandler interface
public:
    virtual int GetCommandCount() override;
    virtual QAction *GetAction(int iActionNo) override;
    virtual void FixButton(int iActionNo, QPushButton *pButton) override;

    virtual void UpdateEditCommandName(QString &rEditButtnName) override;
    virtual void UpdateNewCommandName(QString &rNewCommandName) override;
};

#endif // MANAGECUSTOMERWND_H
