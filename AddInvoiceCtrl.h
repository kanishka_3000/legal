/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef ADDINVOICECTRL_H
#define ADDINVOICECTRL_H

#include <QWidget>
#include "ui_AddInvoiceCtrl.h"
#include <EntityManipulaterCtrl.h>
#include <core/InvoiceDataController.h>
class AddInvoiceCtrl : public EntityManipulaterCtrl,  EntityManipulaterCtrl::PopulationCallback,public Ui::AddInvoiceCtrl
{
    Q_OBJECT

public:
    explicit AddInvoiceCtrl(QWidget *parent = 0);
    ~AddInvoiceCtrl();
    void OnCreateCtrl();
    virtual void PopulateWithEntity(std::shared_ptr<Entity> pEntity);
    virtual void OnPreSave();

public slots:
    void OnClose();
    void OnInvoiceType(int iIndex);

    void OnCaseSelected(QString sCase);
    void onDefaultAdditionalCosts();
    void OnInitialAddStepFilter();
private:

    InvoiceDataController o_InvoiceDataController;
    // GenericCtrl interface
public:
    virtual void OnKey(QString sKey);
protected:
    void SetInvoiceNo();

    // EntityManipulaterCtrl interface
protected:
    virtual void OnPostSave() override;

    // PopulationCallback interface
    void SetCaseModel();
    QString s_InvoiceTrackID;
public:
    virtual void OnPopulation(EntityManipulaterCtrl::PopulationFeedback eType, QString sFieldName, QWidget *pWidget);
};

#endif // ADDSERVICEGROUPCTRL_H
