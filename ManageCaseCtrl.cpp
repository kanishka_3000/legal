/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "ManageCaseCtrl.h"
#include <LGLDBFields.h>
#include <GenericWnd.h>
#include <LGLGlobalDefs.h>
#include <QMessageBox>
ManageCaseCtrl::ManageCaseCtrl() :
    GenericCtrl(nullptr)
{
    setupUi(this);
    p_QueryCtrl->SetTableName(TBLNM_LGL_COURTCASE);
    p_QueryCtrl->SetKeyName(FDNM_LGL_COURTCASE_CASETRACKID);
    p_QueryCtrl->SetOrderColumn(FDNM_LGL_COURTCASE_AC_NO);
    p_QueryCtrl->SetCommandHanlder(this);
    p_QueryCtrl->SetEnableExport(true);
}

ManageCaseCtrl::~ManageCaseCtrl()
{

}

void ManageCaseCtrl::OnAddInvoice()
{
    QString sKey = p_QueryCtrl->GetSelectedKey();
    if(sKey.isEmpty())
    {
        QMessageBox::warning(this, tr("Invalid Selection"), tr("Select a valid case first"));
        return;
    }

    ManageInvoiceWnd* pWnd = nullptr;
    CreateInvoiceWnd(sKey, pWnd);
    pWnd->SetOpenType(OPENTYPE_ADD);
}

void ManageCaseCtrl::OnFindInvoices()
{
    QString sKey = p_QueryCtrl->GetSelectedKey();
    if(sKey.isEmpty())
    {
        QMessageBox::warning(this, tr("Invalid Selection"), tr("Select a valid case first"));
        return;
    }
    ManageInvoiceWnd* pWnd = nullptr;
    CreateInvoiceWnd(sKey, pWnd);
    pWnd->SetOpenType(OPENTYPE_MANAGE);
}

void ManageCaseCtrl::OnCreateCtrl()
{
    connect(p_Filter, SIGNAL(NotifyFilter(QList<FilterCrieteria>)), p_QueryCtrl, SLOT(OnFilter(QList<FilterCrieteria>)));
    connect(p_QueryCtrl,SIGNAL(NotifyNewRequest()), p_Parent, SLOT(OnAddService()));
    connect(p_QueryCtrl,SIGNAL(NotifyEditRequest(QString)),p_Parent,SLOT(OnEditRequest(QString)));
}


int ManageCaseCtrl::GetCommandCount()
{
    return 2;
}

QAction *ManageCaseCtrl::GetAction(int iActionNo)
{
    switch(iActionNo)
    {
    case 0:
    {
        connect(pAct_Add_Invoice, SIGNAL(triggered(bool)),this, SLOT(OnAddInvoice()));
        return pAct_Add_Invoice;
        break;
    }
    case 1:
    {
        connect(pAct_Find_Invoices, SIGNAL(triggered(bool)), this, SLOT(OnFindInvoices()));
        return pAct_Find_Invoices;
        break;
    }
    default:
    {
        Q_ASSERT(false);
    }
    }
    return nullptr;
}

void ManageCaseCtrl::FixButton(int iActionNo, QPushButton *pButton)
{
    QAction* pAction = GetAction(iActionNo);
    switch(iActionNo)
    {
    case 0:
    {
        connect(pButton, SIGNAL(clicked(bool)),this, SLOT(OnAddInvoice()));
        break;
    }
    case 1:
    {
         connect(pButton, SIGNAL(clicked(bool)),this, SLOT(OnFindInvoices()));
        break;
    }
    default:
    {
        Q_ASSERT(false);
    }
    }
    pButton->setIcon(pAction->icon());
    pButton->setText(pAction->text());
}

void ManageCaseCtrl::CreateInvoiceWnd(QString sKey, ManageInvoiceWnd *&pWnd)
{
    pWnd = p_Parent->GetApplication()->CreateWndEx<ManageInvoiceWnd>(WND_INVOICE);
    pWnd->DistributeKeytoChildren(sKey);
}
