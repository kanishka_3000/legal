/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef SUMMERYWND_H
#define SUMMERYWND_H
#include <GenericWnd.h>
#include "ui_SummeryWnd.h"

class SummeryWnd : public GenericWnd, private Ui::SummeryWnd
{
    Q_OBJECT

public:
    explicit SummeryWnd(MainWnd *parent,QString sTitle );
};

#endif // SUMMERYWND_H
