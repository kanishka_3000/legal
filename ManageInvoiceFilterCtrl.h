/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef MANAGEINVOICEFILTERCTRL_H
#define MANAGEINVOICEFILTERCTRL_H

#include "ui_ManageInvoiceFilterCtrl.h"
#include <GenericFilterCtrl.h>
#include <FieldCompleter.h>
class ManageInvoiceFilterCtrl : public GenericFilterCtrl, private Ui::ManageInvoiceFilterCtrl
{
    Q_OBJECT

public:
    explicit ManageInvoiceFilterCtrl(QWidget *parent = 0);

    // GenericCtrl interface
public:
    virtual ~ManageInvoiceFilterCtrl();
    virtual void OnCreateCtrl();

    // GenericCtrl interface
public:
    virtual void OnKey(QString sKey);
private:
    FieldCompleter* p_FieldCompleter;
};

#endif // MANAGEINVOICEFILTERCTRL_H
