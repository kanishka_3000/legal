#include "CustomerRegisterCtrl.h"
#include <GenericWnd.h>
#include <LGLDBFields.h>
#include <QIntValidator>
#include <QRegExpValidator>
#include <LabelAnimator.h>
#include <QMessageBox>
#include <GenericWnd.h>

CustomerRegisterCtrl::CustomerRegisterCtrl(QWidget *parent) :
    EntityManipulaterCtrl(parent)

{
    setupUi(this);
    SetFieldProperty(txt_PlantiffName, FDNM_LGL_CUSTOMER_PLANTIFF_NAME);
    SetFieldProperty(txt_Name, FDNM_LGL_CUSTOMER_NAME);
    SetFieldProperty(txt_ContactName, FDNM_LGL_CUSTOMER_MAIN_CONTACT_NAME);
    SetFieldProperty(txt_ContactPhone, FDNM_LGL_CUSTOMER_MAIN_CONTACT_NO);
    SetFieldProperty(txt_Address, FDNM_LGL_CUSTOMER_ADDRESS);

    b_DeleteWhenDone = false;
    s_EntityType = TBLNM_LGL_CUSTOMER;
    btn_Close->setVisible(false);   

}

CustomerRegisterCtrl::~CustomerRegisterCtrl()
{

}

void CustomerRegisterCtrl::OnCreateCtrl()
{
    SetCurrentDateToAllFields();
//    txt_RegistedBy->setText(p_Parent->GetApplication()->GetUserName());
//    txt_RegistedBy->setReadOnly(true);
//    dt_RegistratinDate->setReadOnly(true);
//    txt_ContactNo->setValidator(new QIntValidator(111111111,999999999,txt_ContactNo));

    connect(btn_Close, SIGNAL(clicked(bool)),this,SLOT(OnClose()));

}

void CustomerRegisterCtrl::PopulateWithEntity(std::shared_ptr<Entity> pEntity)
{
    EntityManipulaterCtrl::PopulateWithEntity(pEntity);
    btn_Close->setVisible(true);
}

bool CustomerRegisterCtrl::CustomValidations(QString &sError)
{
    if(txt_ContactPhone->text().length() < 9)
    {
        sError = tr("Invalid Contact No Length");
        return false;
    }
    return true;
}

void CustomerRegisterCtrl::OnClose()
{
    p_Parent->OnChildRemoveRequest(this);
    deleteLater();
}

bool CustomerRegisterCtrl::OnSave(EntityManipulaterSaveDislpayer *pSaveDisplayer)
{
    (void)pSaveDisplayer;
    CustomerRegisterCallback oReg(lbl_Notification,this);
    return EntityManipulaterCtrl::OnSave(&oReg);
}


void CustomerRegisterCtrl::CustomerRegisterCallback::EntitySaved(bool bEdit)
{
    (void)bEdit;
    p_Label->setText(tr("Customer saved successfully"));
    LabelAnimator oAnim(p_Label,p_Parent);
    oAnim.Animate(LabelAnimator::AnimationType::FADEOUT);

    if(!bEdit)
    {

//        QMessageBox::StandardButton reply = QMessageBox::question(p_Parent, tr("Order?"), tr("Registration Done.  Would you like to place an order?"),  QMessageBox::Yes|QMessageBox::No , QMessageBox::StandardButton::Yes);
//        if(reply == QMessageBox::Yes)
//        {
//            OrderWnd* pOrderWnd = static_cast<OrderWnd*>(Application::GetApplication()->CreateWnd(WND_ORDER));
//            std::shared_ptr<Customer> pCustomer = std::static_pointer_cast<Customer>(p_Parent->p_Entity);
//            pOrderWnd->InitializeWithCustomer(pCustomer);
//        }
    }
    else
    {
        QMessageBox::information(p_Parent, tr("Edit Success"),  tr("Customer details updated"));
    }
}


void CustomerRegisterCtrl::OnPostSave()
{
    if(b_EditMode)
    {
        OnClose();
    }
}
