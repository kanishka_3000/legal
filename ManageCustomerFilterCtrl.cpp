#include "ManageCustomerFilterCtrl.h"
#include <LGLDBFields.h>
#include <GenericWnd.h>
ManageCustomerFilterCtrl::ManageCustomerFilterCtrl(QWidget *parent) :
GenericFilterCtrl(parent)
{
    setupUi(this);
    SetFieldProperty(txt_PlantiffName, FDNM_LGL_CUSTOMER_PLANTIFF_NAME);
    SetFieldProperty(txt_Name, FDNM_LGL_CUSTOMER_NAME);
    SetFieldProperty(txt_ContactName, FDNM_LGL_CUSTOMER_MAIN_CONTACT_NAME);
    SetFieldProperty(txt_Address, FDNM_LGL_CUSTOMER_ADDRESS);
    SetFieldProperty(dt_RegDate, FDNM_LGL_CUSTOMER_REGISTRATION_DATE);
}

ManageCustomerFilterCtrl::~ManageCustomerFilterCtrl()
{

}

void ManageCustomerFilterCtrl::OnCreateCtrl()
{
    SetCurrentDateToAllFields();
    p_Clm = new FieldCompleter(p_Parent->GetApplication(),TBLNM_LGL_CUSTOMER);
    p_Clm->SetSource(txt_Name, FDNM_LGL_CUSTOMER_NAME);
    p_Clm->SetDestination(txt_PlantiffName, FDNM_LGL_CUSTOMER_PLANTIFF_NAME);
   // chk_RegisterDate->setMinimumHeight(lbl_Email->geometry().height()+ 2);
}
