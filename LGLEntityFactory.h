/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef LGLENTITYFACTORY_H
#define LGLENTITYFACTORY_H

#include <EntityFactory.h>
class LGLEntityFactory:public EntityFactory
{
public:
    LGLEntityFactory();
protected:
    virtual void RegisterEntities();
};

#endif // LGLENTITYFACTORY_H
