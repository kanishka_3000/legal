/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef MANAGECASEWND_H
#define MANAGECASEWND_H

#include <QWidget>
#include "ui_ManageCaseWnd.h"
#include <GenericWnd.h>
#include <AddCaseCtrl.h>
class ManageCaseWnd : public GenericWnd, public Ui::ManageCaseWnd
{
    Q_OBJECT

public:
    explicit ManageCaseWnd(MainWnd* pParent, QString sTitle);
    virtual void OnCreate();
    virtual void OnChildRemoveRequest(GenericCtrl* pChild);
    ~ManageCaseWnd();
    void SetOpenType(int iType);
public slots:
    void OnAddService();
    virtual void OnEditRequest(QString sKeyValue);
protected:

private:
    AddCaseCtrl* p_EditCtrl;
};

#endif // MANAGESERVICESGROUPWND_H
