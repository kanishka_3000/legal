/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "ManageInvoiceDataCtrl.h"


ManageInvoiceDataCtrl::ManageInvoiceDataCtrl(QWidget *pParent) :
    GenericCtrl(pParent)
{
    setupUi(this);

}

ManageInvoiceDataCtrl::~ManageInvoiceDataCtrl()
{

}

