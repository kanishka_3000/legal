/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "ManageAdditionalStepCtrl.h"
#include <LGLDBFields.h>
#include <GenericWnd.h>
#include <AddAdditionalStepWnd.h>
#include <LGLGlobalDefs.h>
ManageAdditionalStepCtrl::ManageAdditionalStepCtrl(QWidget *parent) :
    GenericCtrl(parent
                )
{
    setupUi(this);
    p_QueryCtrl->SetConfigName("Manage Additional");
    p_QueryCtrl->SetTableName(TBLNM_LGL_ADDITIONALSTEP);
    p_QueryCtrl->SetKeyName(FDNM_LGL_ADDITIONALSTEP_STEPID);


}

ManageAdditionalStepCtrl::~ManageAdditionalStepCtrl()
{

}

void ManageAdditionalStepCtrl::OnKey(QString sInvoiceTrackID)
{
    p_Filter->OnKey(sInvoiceTrackID);
    s_InvoiceTrackID = sInvoiceTrackID;
}

void ManageAdditionalStepCtrl::ReloadQuery()
{
    p_QueryCtrl->Reload();
}

void ManageAdditionalStepCtrl::OnAddService()
{
    AddAdditionalStepWnd* pAddStep = p_Parent->GetApplication()->CreateWndEx<AddAdditionalStepWnd>(WND_ADDADDITIONAL_STEP);
    pAddStep->RegisterForSaveNotification(p_QueryCtrl);
    pAddStep->DistributeKeytoChildren(s_InvoiceTrackID);
}

void ManageAdditionalStepCtrl::OnEditRequest(QString sEditIt)
{
    AddAdditionalStepWnd* pAddStep = p_Parent->GetApplication()->CreateWndEx<AddAdditionalStepWnd>(WND_ADDADDITIONAL_STEP);
    pAddStep->RegisterForSaveNotification(p_QueryCtrl);
    pAddStep->OnAdditionalItemEditRequest(sEditIt);
}

void ManageAdditionalStepCtrl::ConnectFilter()
{
    connect(p_Filter, SIGNAL(NotifyFilter(QList<FilterCrieteria>)), p_QueryCtrl,
            SLOT(OnFilter(QList<FilterCrieteria>)),Qt::UniqueConnection);
}

void ManageAdditionalStepCtrl::OnCreateCtrl()
{
    ConnectFilter();
    //p_QueryCtrl->Initialize();
    connect(p_QueryCtrl, SIGNAL(NotifyNewRequest()), this, SLOT(OnAddService()));
    connect(p_QueryCtrl, SIGNAL(NotifyEditRequest(QString)), this, SLOT(OnEditRequest(QString)));
}



