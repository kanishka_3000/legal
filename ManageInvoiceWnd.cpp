/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "ManageInvoiceWnd.h"

//#include <crmglobaldefs.h>
#include <ManageInvoiceCtrl.h>
#include <QDebug>
#include <LGLDBFields.h>
ManageInvoiceWnd::ManageInvoiceWnd(MainWnd* pParent, QString sTitle) :
    GenericWnd(pParent, sTitle)
{
    setupUi(this);
    p_EditCtrl = NULL;

}

void ManageInvoiceWnd::OnCreate()
{

     ReSize(730,540);
}

void ManageInvoiceWnd::OnChildRemoveRequest(GenericCtrl *pChild)
{
    if(pChild == p_EditCtrl)
    {
        qDebug() << "Child removed";
        //delete p_EditCtrl;
        p_EditCtrl = NULL;
    }
}

ManageInvoiceWnd::~ManageInvoiceWnd()
{

}

void ManageInvoiceWnd::SetOpenType(int iType)
{
    switch(iType)
    {
    case 2:
    {
        tab_Ctrls->setCurrentIndex(1);
        break;
    }
    case 1:
    {
        tab_Ctrls->setCurrentIndex(0);
    }
    }
}

void ManageInvoiceWnd::OnAddService()
{

    tab_Ctrls->setCurrentIndex(1);
}

void ManageInvoiceWnd::OnEditRequest(QString sKeyValue)
{
//    if(p_EditCtrl)
//    {
//        delete p_EditCtrl;
//    }
    std::shared_ptr<Entity> pEntity = Entity::FindInstance(sKeyValue,TBLNM_LGL_INVOICE);
    p_EditCtrl = new AddInvoiceCtrl(this);
    OnNewChild(p_EditCtrl);
    int iTab = tab_Ctrls->addTab(p_EditCtrl,tr("Edit Group"));
    tab_Ctrls->setCurrentIndex(iTab);
    p_EditCtrl->PopulateWithEntity(pEntity);

}


